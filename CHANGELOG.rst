.. SPDX-FileCopyrightText: 2022-2025 Tobias Leupold <tl@stonemx.de>

   SPDX-License-Identifier: CC-BY-SA-4.0

   The format of this file is inspired by keepachangelog.com, but uses ReStructuredText instead of
   MarkDown. Keep the line length at no more than 100 characters (with the obvious exception of the
   header template below, which needs to be indented by three spaces)

   Here's the header template to be pasted at the top after a new release:

   ====================================================================================================
   [unreleased]
   ====================================================================================================

   Added
   =====

   * for new features.

   Changed
   =======

   * for changes in existing functionality.

   Deprecated
   ==========

   * for soon-to-be removed features.

   Removed
   =======

   * for now removed features.

   Fixed
   =====

   * for any bug fixes.

   Security
   ========

   * in case of vulnerabilities.

====================================================================================================
Scandoc 1.3.0 released (2025-01-11)
====================================================================================================

Changed
=======

* Qt 6 detection is now done like the others do it. A Qt 6 build can be forced by setting ECM's
  :code:`BUILD_WITH_QT6` variable, i.e. by passing :code:`-DBUILD_WITH_QT6=ON` to the cmake call.

* The default post-processing commands now use ImageMagick's new scheme, i.e. now ``magick`` is
  invoked instead of the legacy ``convert`` command.

Fixed
=====

* Scandoc can now be built with Qt 6.8. Remaining deprecation warnings have been fixed.

====================================================================================================
Scandoc 1.2.0 released (2024-10-09)
====================================================================================================

Added
=====

* The program now tracks which pages have been saved (either all at once as PDF, unprocessed or
  processed scans or as a single file) and now warns if any scanned page has not been saved at all
  yet when closing.

Changed
=======

* Resetting (deleting all scanned pages) after saving is now only triggered if a PDF file is saved
  or all processed/unprocessed scans are saved, not if a single page is saved. This prevents other
  possibly scanned pages from being discarded.

* Scandoc now uses Qt6/KF6 by default. It still can be built against Qt5/KF5 however, by disabling
  the :code:`QT6` flag (by passing :code:`-DQT6=OFF` to cmake).

====================================================================================================
Scandoc 1.1.0 released (2024-01-25)
====================================================================================================

Added
=====

* It is now possible to not only save a PDF file for all scanned pages, but also the processed
  (JPEG) and unprocessed (PNG) image of the currently viewed page or all scanned pages.

* Generated PDF files can now also be encrypted. By default, this is achieved by using
  `QPDF <https://qpdf.sourceforge.io/>`_.

====================================================================================================
Scandoc 1.0.0 released (2022-11-09)
====================================================================================================

Added
=====

* Added an example PDF output script generating a searchable PDF with the help of Tesseract.

* Added a custom scan progress widget which also shows the current state of the scanned image.

* Added a status bar and added messages to be shown by it.

* Added a "Delete all pages" button to reset the program if multiple scans with multiple pages are
  done in a row.

* One now can activate automatic resetting (deletion of all pages) after saving the PDF output.

* The user can now choose which folder for saving the PDF output should be pre-selected: Either the
  system's default "Documents" folder, a custom folder or the last used one.

* A pdf filename template can now be defined. It can contain date and time information, as well as
  a placeholder for a consecutive number (which can optionally be omitted for the first file).

* Added a "Duplex scan" settings part. If a device has multiple sources to choose from (and thus
  potentially a duplex unit), one now can define a "one-sided" scan source and a "duplex" source.
  On the "Scanned pages" tab, one then can choose from three "Scan page" actions: The default one
  uses the currently selected source, the two other ones the "one-sided" or the "duplex" one.

* When doing duplex scans, one can now automatically rotate all even pages by 180°. Some devices
  (e.g. the Brother MFC-8880DN) output all even pages upside-down. Which is arguable a bug in the
  backend; however this can be addressed automatically now.

Changed
=======

* Removed the CamelCase spelling: Let's call it Scandoc, not ScanDoc.

* Grouped batch scan options in an own group box, reworked the options handling.

* Ported Scandoc from libksane to KSaneCore.

Removed
=======

* Image rotation is now done via Qt's own tools, so there's no need to have an external "rotate"
  command anymore.

Fixed
=====

* When sending pdf files via email directly, the default file "scan.pdf" was overwritten when
  another scan was done in the same session. Now, the new pdf file name template is used for storing
  to-be-sent files in the temporary directory. If no consecutive number placeholder is given, a
  default one is appended, so that a saved scan is never overwritten.

====================================================================================================
ScanDoc 0.1.0 released (2022-03-06)
====================================================================================================

* Initial release: First state of code seemingly feature-complete and working ;-)
