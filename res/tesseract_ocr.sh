#!/bin/bash

# SPDX-FileCopyrightText: none
#
# SPDX-License-Identifier: CC0-1.0

# This script demonstrates how a searchable OCR processed PDF file can be created using Scandoc.
#
# It uses the Tesseract OCR engine (cf. https://github.com/tesseract-ocr ) to create searchable
# PDF files for all input files and pdfjam (cf. https://github.com/rrthomas/pdfjam ) to combine
# all output files to one PDF file (if more than one page has been scanned).
#
# Scandoc's "PDF saving" command to call this script is:
#
#     /path/to/this/script %IN% %OUT%
#
# This example passes German as the language to use to Tesseract ("-l" switch). Adjust as needed!

# Here we go:

# 1. Invoke Tesseract for all input files

# Array for pdfjam's input file list
in=()

# Iterate over all command line arguments but the last one
# These represent all pre-processed JPEG images
for ((i=1; i<$#; i++)); do
    # Remove the ".jpg" extension from the input and store the result in $out
    out=${!i/.jpg/}
    # Invoke tesseract on the input file and create a searchable PDF file
    tesseract ${!i} $out -l deu pdf
    # Append the PDF file created by Tesseract to the $in array
    in+=($out.pdf)
done

# 2. Create the combined output PDF file

# ${!#} is the last command line argument, which is the name of the PDF output target

if [[ $# -eq 2 ]]; then
    # Only one input file has been passed. We can simply rename Tesseract's output
    mv ${in[0]} ${!#}
else
    # We have multiple processed files and combine them using pdfjam
    pdfjam --a4paper ${in[@]} --outfile ${!#}
fi

# Done :-)
