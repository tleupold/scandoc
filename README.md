# Scandoc

## What is it?

Scandoc is a small utility program written to help doing one specific task: Scanning some document, post processing and compressing the scanned image(s), and save all pages to a single PDF file.

Thus, it doesn't want to compete with general-purpose scanning programs like e.g. [XSane](http://www.xsane.org/) or [Skanlite](https://invent.kde.org/graphics/skanlite). Another KDE project with a similar purpose (and a nifty modern QtQuick UI) is [Skanpage](https://invent.kde.org/utilities/skanpage); it however lacks extensive post-processing options and/or a scripting interface, and that's the raison d'être of Scandoc.

Scandoc itself is written in C++/[Qt](https://www.qt.io/) and uses the [KDE Frameworks](https://api.kde.org/frameworks/). It's published under the terms of the [GNU General Public License (GPL)](https://www.gnu.org/licenses/#GPL).

Scandoc can be compiled both against Qt5/KF5 as well as Qt6/KF6. By default, The Qt6/KF6 build is done. To build against Qt5/KF5, disable the `QT6` switch (i.e. `cmake -DQT6=OFF ...`).

## How does it work?

Scandoc uses [KSaneCore](https://invent.kde.org/libraries/ksanecore) to access a scanner, and runs external helper programs to do the post processing, compression and conversion. This way, well-known Linux console tools can be used for those tasks, without re-inventing the wheel. Pages can be previewed, deleted, rotated by 180° and re-arranged before creating the output PDF file.

By default, [ImageMagick](https://imagemagick.org/)'s [magick](https://imagemagick.org/script/magick.php) is used to adjust the contrast of a scanned page and to compress it to a JPEG image, and [pdfjam](https://github.com/rrthomas/pdfjam) (which is e.g. packaged by [TeX Live](https://www.tug.org/texlive/)) is used to create a PDF file from all post processed scanned pages.

However, neither ImageMagick nor pdfjam are hard dependencies. Arbitrary helper programs (like shell scripts doing more like deskewing, OCR etc.) can be used as well. A bash script creating a searchable PDF output with the help of the [Tesseract OCR engine](https://github.com/tesseract-ocr) along with pdfjam can be found in `res/tesseract_ocr.sh` as an example.

## Why should I use it?

It does one task, and it does it well ;-)

Once having selected reasonable settings for your scanner (like A4 paper size, 150 dpi resolution, grayscale mode or whatever suits your needs) and for the post-processing commands used (you may simply want to use the presets), creating a reasonably small PDF from some document and sending it attached to an email is as easy as clicking "Scan page" and "Save PDF and send via email" when the scan is done.

If your device has an automatic document feeder and/or a duplex unit, the respective sources can be defined, so that you can easily start a flat-bed scan, an ADF one-sided scan or an ADF duplex scan by simply selecting the respective setting from the "Scan page" button's menu before clicking it.

Scandoc stores all automatically-created files in a temporary directory that is deleted when the program closes, so there's no need to clean up after having used it. Close it and you're done.

Scandoc simply automates an everyday task as much as possible and makes it as straightforward as possible.
