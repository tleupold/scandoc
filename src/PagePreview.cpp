// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "PagePreview.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QVBoxLayout>
#include <QScrollArea>
#include <QLabel>
#include <QHBoxLayout>
#include <QToolButton>
#include <QIcon>
#include <QScrollBar>
#include <QTimer>
#include <QPalette>
#include <QDebug>

PagePreview::PagePreview(QWidget *parent) : QWidget(parent)
{
    auto *layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    m_scrollArea = new QScrollArea;
    auto palette = m_scrollArea->palette();
    palette.setColor(QPalette::Base, palette.color(QPalette::Dark));
    m_scrollArea->setPalette(palette);
    m_preview = new QLabel;
    m_scrollArea->setWidget(m_preview);
    layout->addWidget(m_scrollArea);

    auto *zoomLayout = new QVBoxLayout;
    layout->addLayout(zoomLayout);

    m_zoomToFitButton = new QToolButton;
    m_zoomToFitButton->setIcon(QIcon::fromTheme(QStringLiteral("zoom-fit-best")));
    m_zoomToFitButton->setToolTip(i18n("Zoom to fit the current window size"));
    connect(m_zoomToFitButton, &QToolButton::clicked, this, &PagePreview::zoomToFit);
    zoomLayout->addWidget(m_zoomToFitButton);
    m_zoomToFitButton->setEnabled(false);

    m_zoomToOriginalButton = new QToolButton;
    m_zoomToOriginalButton->setIcon(QIcon::fromTheme(QStringLiteral("zoom-original")));
    m_zoomToOriginalButton->setToolTip(i18n("Zoom to the scanned page's original size"));
    connect(m_zoomToOriginalButton, &QToolButton::clicked, this, &PagePreview::zoomToOriginal);
    zoomLayout->addWidget(m_zoomToOriginalButton);
    m_zoomToOriginalButton->setEnabled(false);

    m_zoomInButton = new QToolButton;
    m_zoomInButton->setIcon(QIcon::fromTheme(QStringLiteral("zoom-in")));
    m_zoomInButton->setToolTip(i18n("Zoom in"));
    connect(m_zoomInButton, &QToolButton::clicked, this, &PagePreview::zoomIn);
    zoomLayout->addWidget(m_zoomInButton);
    m_zoomInButton->setEnabled(false);

    m_zoomOutButton = new QToolButton;
    m_zoomOutButton->setIcon(QIcon::fromTheme(QStringLiteral("zoom-out")));
    m_zoomOutButton->setToolTip(i18n("Zoom out"));
    connect(m_zoomOutButton, &QToolButton::clicked, this, &PagePreview::zoomOut);
    zoomLayout->addWidget(m_zoomOutButton);
    m_zoomOutButton->setEnabled(false);

    zoomLayout->addStretch();
}

void PagePreview::setImage(const QString &path)
{
    if (path.isEmpty()) {
        m_preview->setPixmap(QPixmap());
        m_zoomToFitButton->setEnabled(false);
        m_zoomToOriginalButton->setEnabled(false);
        m_zoomInButton->setEnabled(false);
        m_zoomOutButton->setEnabled(false);
        return;
    }

    m_image.load(path);
    zoomToFit();

    m_zoomToFitButton->setEnabled(true);
    m_zoomToOriginalButton->setEnabled(true);
    m_zoomInButton->setEnabled(true);
    m_zoomOutButton->setEnabled(true);
}

void PagePreview::updatePreview()
{
    const auto pixmap = QPixmap::fromImage(m_image.scaled(m_preview->size(),
                                                          Qt::KeepAspectRatio,
                                                          Qt::SmoothTransformation));
    m_preview->setPixmap(pixmap);
    m_preview->resize(pixmap.size());
}

QSize PagePreview::maximalViewSize() const
{
    auto size = m_scrollArea->viewport()->size();
    auto *horizontalScrollBar = m_scrollArea->horizontalScrollBar();
    if (horizontalScrollBar->isVisible()) {
        size.setHeight(size.height() + horizontalScrollBar->height());
    }
    auto *verticalScrollBar = m_scrollArea->verticalScrollBar();
    if (verticalScrollBar->isVisible()) {
        size.setWidth(size.width() + verticalScrollBar->width());
    }
    return size;
}

void PagePreview::zoomToFit()
{
    m_preview->resize(maximalViewSize());
    updatePreview();
    toggleZoomButtons();
}

void PagePreview::zoomToOriginal()
{
    m_preview->resize(m_image.size());
    m_preview->setPixmap(QPixmap::fromImage(m_image));
    toggleZoomButtons();
}

void PagePreview::zoomIn()
{
    QSize size;
    size.setWidth(m_preview->width() * 1.25);
    size.setHeight(m_preview->height() * 1.25);
    m_preview->resize(size);
    updatePreview();
    toggleZoomButtons();
}

void PagePreview::zoomOut()
{
    QSize size;
    size.setWidth(m_preview->width() * 0.8);
    size.setHeight(m_preview->height() * 0.8);

    // Don't show the page smaller as it could be shown
    const auto availableViewSize = maximalViewSize();
    if (size.width() <= availableViewSize.width() && size.height() <= availableViewSize.height())
    {
        zoomToFit();
        return;
    }

    m_preview->resize(size);
    updatePreview();
    toggleZoomButtons();
}

void PagePreview::toggleZoomButtons()
{
    if (m_image.isNull()) {
        return;
    }

    // We do this inside a QTimer::singleShot to be sure the visibility state of the scroll bars
    // already has been updated (and they are hidden if they have been shows before but not anymore)

    QTimer::singleShot(0, this, [this]
    {
        auto previewWidth = m_preview->width();
        auto previewHeight = m_preview->height();
        auto visibleWidth = m_scrollArea->viewport()->width();
        auto visibleHeight = m_scrollArea->viewport()->height();

        m_zoomOutButton->setEnabled(previewWidth > visibleWidth || previewHeight > visibleHeight);
        m_zoomInButton->setEnabled(   previewWidth < m_image.width() * 3
                                   && previewHeight < m_image.height() * 3);
    });
}

void PagePreview::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);
    toggleZoomButtons();
}
