// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "SavingWidget.h"
#include "Settings.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QRadioButton>
#include <QDebug>
#include <QFileDialog>
#include <QLineEdit>

// C++ includes
#include <functional>

SavingWidget::SavingWidget(SharedObjects *sharedObjects, QWidget *parent)
    : AbstractSettingsWidget(sharedObjects, parent)
{
    QGroupBox *box;
    QVBoxLayout *boxLayout;
    QLabel *label;

    // Default save folder

    box = new QGroupBox(i18n("Default save folder"));
    boxLayout = new QVBoxLayout(box);
    pageLayout()->addWidget(box);

    m_documentsFolderTarget = new QRadioButton(i18n("Set the default \"Documents\" folder"));
    boxLayout->addWidget(m_documentsFolderTarget);

    m_customFolderTarget = new QRadioButton(i18n("Set the following folder (select to select "
                                                 "one)"));
    connect(m_customFolderTarget, &QRadioButton::clicked, this, &SavingWidget::selectCustomFolder);
    boxLayout->addWidget(m_customFolderTarget);

    m_customFolder = new QLabel(settings()->defaultTargetFolder());
    m_customFolder->setIndent(30);
    m_customFolder->setWordWrap(true);
    boxLayout->addWidget(m_customFolder);
    m_customFolder->hide();

    m_lastUsedFolderTarget = new QRadioButton(tr("Set the last used folder"));
    boxLayout->addWidget(m_lastUsedFolderTarget);

    selectSaveTarget();

    // Output filename template

    box = new QGroupBox(i18n("Output filename template"));
    boxLayout = new QVBoxLayout(box);

    m_outputFileNameTemplate = new QLineEdit;
    m_outputFileNameTemplate->setText(settings()->outputFileNameTemplate());
    connect(m_outputFileNameTemplate, &QLineEdit::textChanged,
            this, &SavingWidget::showExampleFilename);
    boxLayout->addWidget(m_outputFileNameTemplate);

    m_exampleFileName = new QLabel;
    boxLayout->addWidget(m_exampleFileName);
    showExampleFilename();

    label = new QLabel(i18n(
        "<p><b>Syntax:</b></p>"
        "<ul>"
        "<li>A placeholder for a consecutive number can be defined by one or more \"n\" and "
            "optionally additional characters enclosed by two \"%\". The number of \"n\" "
            "characters defines how many leading zeros should be inserted.</li>"
        "<li>A literal \"%\" can be added by typing \"%%\"</li>"
        "<li>Date and time information can be added (e.g. \"yyyy\" for a four-digit year). "
            "Everything enclosed in single quotes (\"'\") is not processed by this. See Qt's "
            "documentation about "
            "<a href=\"https://doc.qt.io/qt-5/qdate.html#toString-2\">QDate</a> and "
            "<a href=\"https://doc.qt.io/qt-5/qtime.html#toString\">QTime</a> for details.</li>"
        "</ul>"));
    label->setWordWrap(true);
    label->setOpenExternalLinks(true);
    boxLayout->addWidget(label);

    m_omitFirstNumber = new QCheckBox(i18n("Omit the number for the first file"));
    m_omitFirstNumber->setChecked(settings()->omitFirstNumber());
    boxLayout->addWidget(m_omitFirstNumber);

    pageLayout()->addWidget(box);

    // Automatic resetting

    box = new QGroupBox(i18n("Automatic resetting"));
    boxLayout = new QVBoxLayout(box);
    m_resetAfterSaving = new QCheckBox(i18n(
        "Automatically reset (delete all pages) when all pages have been saved\n"
        "(by saving a PDF file containing all pages or saving all scans as single files)"));
    m_resetAfterSaving->setChecked(settings()->resetAfterSaving());
    boxLayout->addWidget(m_resetAfterSaving);
    pageLayout()->addWidget(box);

    pageLayout()->addStretch();

    addApplyButton();
    connect(m_documentsFolderTarget, &QRadioButton::clicked,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_documentsFolderTarget, &QRadioButton::clicked, m_customFolder, &QLabel::hide);
    connect(m_customFolderTarget, &QRadioButton::clicked,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_lastUsedFolderTarget, &QRadioButton::clicked,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_lastUsedFolderTarget, &QRadioButton::clicked, m_customFolder, &QLabel::hide);
    connect(m_outputFileNameTemplate, &QLineEdit::textChanged,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_omitFirstNumber, &QCheckBox::toggled,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_resetAfterSaving, &QCheckBox::toggled,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
}

void SavingWidget::selectSaveTarget()
{
    switch (settings()->defaultSaveTarget()) {
    case Settings::TargetDocumentsFolder:
        m_documentsFolderTarget->setChecked(true);
        break;
    case Settings::TargetCustomFolder:
        m_customFolderTarget->setChecked(true);
        m_customFolder->show();
        break;
    case Settings::TargetLastUsedFolderTarget:
        m_lastUsedFolderTarget->setChecked(true);
        break;
    }
}

void SavingWidget::selectCustomFolder()
{
    const auto folder = QFileDialog::getExistingDirectory(this, i18n("Please select a folder"),
                                                          m_customFolder->text());
    if (folder.isEmpty()) {
        selectSaveTarget();
        return;
    }

    m_customFolder->setText(folder);
    m_customFolder->show();
}

QString SavingWidget::fileNameTemplate() const
{
    const auto text = m_outputFileNameTemplate->text().simplified();
    return text.isEmpty() ? QStringLiteral("'scan'%_nn%") : text;
}

void SavingWidget::showExampleFilename()
{
    m_exampleFileName->setText(i18n("Example output: \"%1.pdf\"",
                                    settings()->outputFileName(fileNameTemplate(), 3)));
}

void SavingWidget::saveSettings()
{
    if (m_documentsFolderTarget->isChecked()) {
        settings()->saveDefaultSaveTarget(Settings::TargetDocumentsFolder);
        settings()->saveDefaultTargetFolder(QString());
    } else if (m_customFolderTarget->isChecked()) {
        settings()->saveDefaultSaveTarget(Settings::TargetCustomFolder);
        settings()->saveDefaultTargetFolder(m_customFolder->text());
    } else if (m_lastUsedFolderTarget->isChecked()) {
        settings()->saveDefaultSaveTarget(Settings::TargetLastUsedFolderTarget);
        settings()->saveDefaultTargetFolder(QString());
    }
    settings()->saveOutputFileNameTemplate(fileNameTemplate());
    settings()->saveOmitFirstNumber(m_omitFirstNumber->isChecked());
    settings()->saveResetAfterSaving(m_resetAfterSaving->isChecked());
}
