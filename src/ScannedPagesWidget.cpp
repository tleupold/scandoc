// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "ScannedPagesWidget.h"
#include "SharedObjects.h"
#include "PagePreview.h"
#include "ProcessHelper.h"
#include "Settings.h"
#include "BusyHelper.h"
#include "PasswordDialog.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QDebug>
#include <QTemporaryDir>
#include <QToolButton>
#include <QIcon>
#include <QLabel>
#include <QMessageBox>
#include <QFileDialog>
#include <QTimer>
#include <QStackedLayout>
#include <QProgressBar>
#include <QRegularExpression>
#include <QDir>
#include <QTransform>
#include <QMenu>
#include <QFile>

// C++ includes
#include <functional>

static const QLatin1String s_asterisk("*");

static const QLatin1String s_pdf(".pdf");
static const QLatin1String s_png(".png");
static const QLatin1String s_jpg(".jpg");

static const QLatin1String s_inPlaceholder("%IN%");
static const QLatin1String s_outPlaceholder("%OUT%");
static const QLatin1String s_pdfPlaceholder("%PDF%");
static const QLatin1String s_passwordPlaceholder("%PASS%");

ScannedPagesWidget::ScannedPagesWidget(SharedObjects *sharedObjects, QWidget *parent)
    : QWidget(parent),
      m_settings(sharedObjects->settings()),
      m_saneInterface(sharedObjects->saneInterface())
{
    m_tmpDir = new QTemporaryDir; // Cleaned up in destructor

    m_rotate.rotate(180);

    // KSaneCore communication
    connect(m_saneInterface, &KSaneCore::Interface::scanFinished,
            this, &ScannedPagesWidget::scanFinished);
    connect(m_saneInterface, &KSaneCore::Interface::scannedImageReady,
            this, &ScannedPagesWidget::pageScanned);
    connect(m_saneInterface, &KSaneCore::Interface::userMessage,
            this, &ScannedPagesWidget::kSaneMessage);
    connect(m_saneInterface, &KSaneCore::Interface::batchModeCountDown,
            this, &ScannedPagesWidget::batchModeCountDown);

    // Widget for page preview, navigation, editing and saving
    // =======================================================

    m_displayWidget = new QWidget;
    auto *displayLayout = new QVBoxLayout(m_displayWidget);

    // Page x/y display
    m_pageLabel = new QLabel;
    displayLayout->addWidget(m_pageLabel);

    // Navigation, scan, control buttons

    auto *controlLayout = new QHBoxLayout;
    displayLayout->addLayout(controlLayout);

    m_previousPageButton = new QToolButton;
    m_previousPageButton->setIcon(QIcon::fromTheme(QStringLiteral("go-previous")));
    m_previousPageButton->setToolTip(i18n("Go to the previous page"));
    connect(m_previousPageButton, &QToolButton::clicked,
            this, [this]
            {
                showPage(--m_currentPageIndex);
            });
    controlLayout->addWidget(m_previousPageButton);

    m_nextPageButton = new QToolButton;
    m_nextPageButton->setIcon(QIcon::fromTheme(QStringLiteral("go-next")));
    m_nextPageButton->setToolTip(i18n("Go to the next page"));
    connect(m_nextPageButton, &QToolButton::clicked,
            this, [this]
            {
                showPage(++m_currentPageIndex);
            });
    controlLayout->addWidget(m_nextPageButton);

    m_scanButton = new QToolButton;
    m_scanButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    m_scanButton->setText(i18n("Scan page"));
    m_scanButton->setIcon(QIcon::fromTheme(QStringLiteral("document-scan")));
    m_scanButton->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Preferred);
    connect(m_scanButton, &QToolButton::clicked, this, &ScannedPagesWidget::startScan);
    controlLayout->addWidget(m_scanButton);

    m_duplexMenu = new QMenu(this);
    auto *defaultScan = m_duplexMenu->addAction(i18n("Scan page (selected source)"));
    connect(defaultScan, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::updateScanMode, this, Scandoc::DefaultScanMode));
    auto *oneSidedScan = m_duplexMenu->addAction(i18n("Scan page (one-sided)"));
    connect(oneSidedScan, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::updateScanMode, this, Scandoc::OneSidedScanMode));
    auto *duplexScan = m_duplexMenu->addAction(i18n("Scan page (duplex)"));
    connect(duplexScan, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::updateScanMode, this, Scandoc::DuplexScanMode));

    m_moveBackwardsButton = new QToolButton;
    m_moveBackwardsButton->setIcon(QIcon::fromTheme(QStringLiteral("go-previous-skip")));
    m_moveBackwardsButton->setToolTip(i18n("Move the current page one backwards"));
    connect(m_moveBackwardsButton, &QToolButton::clicked,
            this, [this]
            {
                movePage(m_currentPageIndex - 1);
            });
    controlLayout->addWidget(m_moveBackwardsButton);

    m_moveForthButton = new QToolButton;
    m_moveForthButton->setIcon(QIcon::fromTheme(QStringLiteral("go-next-skip")));
    m_moveForthButton->setToolTip(i18n("Move the current page one forth"));
    connect(m_moveForthButton, &QToolButton::clicked,
            this, [this]
            {
                movePage(m_currentPageIndex + 1);
            });
    controlLayout->addWidget(m_moveForthButton);

    m_rotateButton = new QToolButton;
    m_rotateButton->setIcon(QIcon::fromTheme(QStringLiteral("circular-arrow-shape")));
    m_rotateButton->setToolTip(i18n("Rotate the current page by 180°"));
    connect(m_rotateButton, &QToolButton::clicked, this, &ScannedPagesWidget::rotatePage);
    controlLayout->addWidget(m_rotateButton);

    m_deleteButton = new QToolButton;
    m_deleteButton->setIcon(QIcon::fromTheme(QStringLiteral("delete")));
    m_deleteButton->setToolTip(i18n("Delete the current page"));
    connect(m_deleteButton, &QToolButton::clicked, this, &ScannedPagesWidget::deletePage);
    controlLayout->addWidget(m_deleteButton);

    m_deleteAllButton = new QToolButton;
    m_deleteAllButton->setIcon(QIcon::fromTheme(QStringLiteral("document-close")));
    m_deleteAllButton->setToolTip(i18n("Delete all pages"));
    connect(m_deleteAllButton, &QToolButton::clicked,
            this, std::bind(&ScannedPagesWidget::deleteAllPages, this, true));
    controlLayout->addWidget(m_deleteAllButton);

    // Page preview
    m_pagePreview = new PagePreview;
    displayLayout->addWidget(m_pagePreview);

    // Save buttons

    auto *saveLayout = new QHBoxLayout;
    displayLayout->addLayout(saveLayout);

    m_encryptPdfButton = new QPushButton(i18n("Encrypt PDF"));
    m_encryptPdfButton->setIcon(QIcon::fromTheme(QStringLiteral("document-encrypted")));
    m_encryptPdfButton->setCheckable(true);
    saveLayout->addWidget(m_encryptPdfButton);

    saveLayout->addStretch();

    m_saveButton = new QToolButton;
    m_saveButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    m_saveButton->setText(i18n("Save PDF"));
    m_saveButton->setIcon(QIcon::fromTheme(QStringLiteral("document-save")));
    connect(m_saveButton, &QPushButton::clicked, this, &ScannedPagesWidget::getSaveFileName);
    saveLayout->addWidget(m_saveButton);

    auto *saveMenu = new QMenu(this);

    auto *savePdf = saveMenu->addAction(i18n("Save PDF"));
    connect(savePdf, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::updateSaveMode, this, Scandoc::SavePdf));
    auto *saveProcessed = saveMenu->addAction(i18n("Save processed page"));
    connect(saveProcessed, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::updateSaveMode, this, Scandoc::SaveProcessedPage));
    auto *saveUnprocessed = saveMenu->addAction(i18n("Save unprocessed page"));
    connect(saveUnprocessed, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::updateSaveMode, this,
                            Scandoc::SaveUnprocessedPage));

    saveMenu->addSeparator();
    auto *saveAllImages = saveMenu->addMenu(i18n("Save all scanned pages"));
    auto *saveAllUnprocessed = saveAllImages->addAction(i18n("Unprocessed images"));
    connect(saveAllUnprocessed, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::saveFiles, this, Scandoc::SaveUnprocessedPage));
    auto *saveAllProcessed = saveAllImages->addAction(i18n("Processed images"));
    connect(saveAllProcessed, &QAction::triggered,
            this, std::bind(&ScannedPagesWidget::saveFiles, this, Scandoc::SaveProcessedPage));

    m_saveButton->setMenu(saveMenu);
    m_saveButton->setPopupMode(QToolButton::MenuButtonPopup);

    m_sendPdfButton = new QPushButton(i18n("Save PDF and send via mail"));
    m_sendPdfButton->setIcon(QIcon::fromTheme(QStringLiteral("document-share")));
    connect(m_sendPdfButton, &QPushButton::clicked, this, &ScannedPagesWidget::sendPdf);
    saveLayout->addWidget(m_sendPdfButton);

    // Widget for the scan progress and preview
    // ========================================

    m_scanWidget = new QWidget;
    auto *scanLayout = new QVBoxLayout(m_scanWidget);

    auto *statusLayout = new QHBoxLayout;
    scanLayout->addLayout(statusLayout);

    m_scanLabel = new QLabel;
    m_scanLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    statusLayout->addWidget(m_scanLabel);

    auto *cancelScanButton = new QPushButton(i18n("Cancel scan"));
    cancelScanButton->setIcon(QIcon::fromTheme(QStringLiteral("dialog-cancel")));
    connect(cancelScanButton, &QPushButton::clicked, this, &ScannedPagesWidget::cancelScan);
    statusLayout->addStretch();
    statusLayout->addWidget(cancelScanButton);

    m_progressBar = new QProgressBar;
    m_progressBar->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Maximum);
    connect(m_saneInterface, &KSaneCore::Interface::scanProgress,
            this, &ScannedPagesWidget::updateScanProgress);
    scanLayout->addWidget(m_progressBar);

    m_previewImage = new QLabel;
    m_previewImage->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    m_previewImage->setAutoFillBackground(true);
    auto palette = m_previewImage->palette();
    palette.setColor(QPalette::Window, palette.color(QPalette::Dark));
    m_previewImage->setPalette(palette);
    scanLayout->addWidget(m_previewImage);

    // ==================================================

    // Assemble the display widget and the scan widget in a QStackedLayout
    m_stackedLayout = new QStackedLayout(this);
    m_stackedLayout->addWidget(m_displayWidget);
    m_stackedLayout->addWidget(m_scanWidget);

    // Disable everything but the "scan" button
    updatePageNavigation();
}

ScannedPagesWidget::~ScannedPagesWidget()
{
    m_saneInterface->stopScan();
    m_saneInterface->closeDevice();
    delete m_tmpDir;
}

void ScannedPagesWidget::startScan()
{
    Q_EMIT showMessage(i18n("Starting scan ..."));
    Q_EMIT scanning(true);
    m_previewImage->clear();
    m_stackedLayout->setCurrentWidget(m_scanWidget);
    updateScanLabel();
    m_scanSeriesCount = 0;
    Q_EMIT requestStartScan(m_scanMode);
}

void ScannedPagesWidget::updateScanLabel()
{
    const auto currentPage = m_currentPageIndex == -1 ? 1 : m_currentPageIndex + 2;
    m_scanLabel->setText(i18n("Scanning page %1 ...", currentPage));
    m_progressBar->setValue(0);
}

void ScannedPagesWidget::batchModeCountDown(int seconds)
{
    if (seconds > 0) {
        m_scanLabel->setText(i18np("Starting next scan in %1 second",
                                   "Starting next scan in %1 seconds", seconds));
    } else {
        updateScanLabel();
    }
}

void ScannedPagesWidget::cancelScan()
{
    m_saneInterface->stopScan();
    m_stackedLayout->setCurrentWidget(m_displayWidget);
    Q_EMIT showMessage(i18n("Scan canceled"));
}

void ScannedPagesWidget::scanFinished(KSaneCore::Interface::ScanStatus, const QString &)
{
    m_stackedLayout->setCurrentWidget(m_displayWidget);
    Q_EMIT scanning(false);
}

void ScannedPagesWidget::pageScanned(const QImage &scannedImage)
{
    m_scanSeriesCount++;
    m_pagesCount++;
    const auto basename = m_tmpDir->filePath(QString::number(m_pagesCount));
    const auto in = basename + s_png;
    const auto out = basename + s_jpg;

    if (m_scanMode == Scandoc::DuplexScanMode
        && m_settings->rotateEvenPages()
        && m_scanSeriesCount % 2 == 0) {

        // Also update the preview to reflect the rotated image
        m_previewImage->setPixmap(m_previewImage->pixmap().transformed(m_rotate));

        scannedImage.transformed(m_rotate).save(in, "PNG");

    } else {
        scannedImage.save(in, "PNG");
    }

    if (! postProcess(in, out)) {
        return;
    }

    m_notSaved.append(basename);
    m_pages.insert(++m_currentPageIndex, basename);
    m_pagePreview->setImage(out);
    updatePageNavigation();
    updateScanLabel();
}

bool ScannedPagesWidget::postProcess(const QString &in, const QString &out)
{
    const auto currentPage = m_currentPageIndex == -1 ? 1 : m_currentPageIndex + 2;
    Q_EMIT showMessage(i18n("Post processing page %1", currentPage));
    ProcessHelper processHelper(m_settings->postProcessingCommand());
    connect(&processHelper, &ProcessHelper::errorMessage,
            this, &ScannedPagesWidget::processErrorMessage);
    processHelper.addValue(s_inPlaceholder, in);
    processHelper.addValue(s_outPlaceholder, out);
    const auto success = processHelper.run();
    Q_EMIT showMessage(i18n("Post processed page %1", currentPage));
    return success;
}

void ScannedPagesWidget::updatePageNavigation()
{
    const auto pagesCount = m_pages.count();

    m_previousPageButton->setEnabled(m_currentPageIndex > 0);
    m_nextPageButton->setEnabled(m_currentPageIndex != -1
                                 && m_currentPageIndex < pagesCount - 1);
    m_deleteButton->setEnabled(m_currentPageIndex != -1);
    m_deleteAllButton->setEnabled(m_currentPageIndex != -1);
    m_moveBackwardsButton->setEnabled(m_currentPageIndex > 0);
    m_moveForthButton->setEnabled(m_currentPageIndex != -1
                                  && m_currentPageIndex < pagesCount - 1);
    m_rotateButton->setEnabled(pagesCount > 0);

    if (pagesCount == 0) {
        m_pageLabel->setText(i18n("<i>No pages scanned yet</i>"));
    } else {
        m_pageLabel->setText(i18n("Page %1 of %2", m_currentPageIndex + 1, pagesCount));
    }

    m_saveButton->setEnabled(pagesCount > 0);
    m_sendPdfButton->setEnabled(pagesCount > 0);
}

void ScannedPagesWidget::showPage(int index)
{
    m_currentPageIndex = index;
    m_pagePreview->setImage(m_pages.at(m_currentPageIndex) + s_jpg);
    updatePageNavigation();
}

void ScannedPagesWidget::deletePage()
{
    if (QMessageBox::question(this, i18n("Delete page"), i18n("Really delete the current page?"),
                              QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
        != QMessageBox::Yes) {

        return;
    }

    m_notSaved.removeOne(m_pages.at(m_currentPageIndex));

    const auto currentPage = m_currentPageIndex + 1;
    m_pages.remove(m_currentPageIndex);
    if (m_currentPageIndex > 0) {
        m_currentPageIndex--;
    } else {
        if (m_pages.count() > 0) {
            m_currentPageIndex = 0;
        } else {
            m_currentPageIndex = -1;
        }
    }
    updatePageNavigation();

    if (m_currentPageIndex != -1) {
        m_pagePreview->setImage(m_pages.at(m_currentPageIndex));
    } else {
        m_pagePreview->setImage(QString());
    }

    Q_EMIT showMessage(i18n("Deleted page %1", currentPage));
}

void ScannedPagesWidget::deleteAllPages(bool ask)
{
    if (ask && QMessageBox::question(this, i18n("Delete all pages"),
                                     i18n("Really delete all pages?"),
                                     QMessageBox::Yes | QMessageBox::No, QMessageBox::No)
        != QMessageBox::Yes) {

        return;
    }

    m_notSaved.clear();
    m_pages.clear();
    m_currentPageIndex = -1;
    m_pagePreview->setImage(QString());
    updatePageNavigation();

    if (ask) {
        Q_EMIT showMessage(i18n("Deleted all pages"));
    }
}

void ScannedPagesWidget::movePage(int target)
{
    const auto page = m_pages.at(m_currentPageIndex);
    m_pages.remove(m_currentPageIndex);
    m_pages.insert(target, page);
    Q_EMIT showMessage(i18n("Moved page %1 to %2", m_currentPageIndex + 1, target + 1));
    showPage(target);
}

void ScannedPagesWidget::rotatePage()
{
    const auto basename = m_pages.at(m_currentPageIndex);
    const auto in = basename + s_png;
    const auto out = basename + s_jpg;

    Q_EMIT showMessage(i18n("Rotating page %1 by 180°", m_currentPageIndex + 1));
    const QImage png(in);
    png.transformed(m_rotate).save(in, "PNG");

    Q_EMIT showMessage(i18n("Post processing page %1", m_currentPageIndex + 1));
    postProcess(in, out);

    Q_EMIT showMessage(i18n("Post processed page %1", m_currentPageIndex + 1));

    showPage(m_currentPageIndex);
}

void ScannedPagesWidget::kSaneMessage(KSaneCore::Interface::ScanStatus status,
                                      const QString &message)
{
    switch (status) {
    case KSaneCore::Interface::Information:
        Q_EMIT showMessage(message);
        break;
    case KSaneCore::Interface::NoError:
        QMessageBox::information(this, i18n("KSane message"), message);
        break;
    case KSaneCore::Interface::ErrorGeneral:
    default:
        QMessageBox::warning(this, i18n("KSane message"), message);
        break;
    }
}

int ScannedPagesWidget::nextFileNumber(const QString &target, const QString &extension,
                                       bool forceNumber) const
{
    const auto wildcard = m_settings->outputFileNameWildcard(forceNumber).append(extension);
    const auto asteriskIndex = wildcard.indexOf(s_asterisk);
    const auto files = QDir(target).entryList(QStringList { wildcard });

    if (asteriskIndex == -1 || files.count() == 0) {
        return 1;
    }

    int number = -1;

    const QRegularExpression re(QRegularExpression::escape(wildcard.mid(-1, asteriskIndex + 1))
                                + QLatin1String("\\D*?(\\d+)\\D*?")
                                + QRegularExpression::escape(
                                        wildcard.mid(asteriskIndex + 1, -1)));

    for (const auto &file : files) {
        const auto match = re.match(file);
        // We set number to 1 in case we don't have a match, which
        // means that the file didn't contain a consecutive number
        int matchedNumber = 1;
        if (match.hasMatch()) {
            matchedNumber = match.captured(1).toInt();
        }

        if (number < matchedNumber) {
            number = matchedNumber;
        }
    }

    if (number == -1) {
        return 1;
    } else {
        return number + 1;
    }
}

void ScannedPagesWidget::getSaveFileName()
{
    QString extension;
    QString title;
    QString filter;

    switch (m_saveMode) {
    case Scandoc::SavePdf:
        extension = s_pdf;
        title = i18n("Please select a file name for the PDF output");
        filter = i18n("PDF files") + QLatin1String(" (*.pdf);;");
        break;
    case Scandoc::SaveProcessedPage:
        extension = s_jpg;
        title = i18n("Please select a file name for the scanned file");
        filter = i18n("JPEG files") + QLatin1String(" (*.jpg *.jpeg);;");
        break;
    case Scandoc::SaveUnprocessedPage:
        extension = s_png;
        title = i18n("Please select a file name for the scanned file");
        filter = i18n("PNG files") + QLatin1String(" (*.png);;");
        break;
    }

    filter.append(i18n("All files") + QLatin1String(" (*)"));

    const auto target = m_settings->saveTargetFolder();
    const auto number = nextFileNumber(target, extension);

    QString targetFile;
    if (target.isEmpty()) {
        targetFile = m_settings->outputFileName(number) + extension;
    } else {
        targetFile = target + QStringLiteral("/") + m_settings->outputFileName(number)
                            + extension;
    }

    const auto fileName = QFileDialog::getSaveFileName(this, title, targetFile, filter);
    if (fileName.isEmpty()) {
        return;
    }

    if (! saveFile(fileName)) {
        return;
    } else {
        if (m_settings->resetAfterSaving() && m_saveMode == Scandoc::SavePdf) {
            deleteAllPages(false);
        }
    }

    if (m_settings->defaultSaveTarget() == Settings::TargetLastUsedFolderTarget) {
        const QFileInfo info(fileName);
        m_settings->saveDefaultTargetFolder(info.dir().path());
    }

    QString text;

    switch (m_saveMode) {
    case Scandoc::SavePdf:
        title = i18n("PDF saved");
        text = i18n("The scanned pages have been saved to <kbd>%1</kbd>", fileName);
        break;
    case Scandoc::SaveProcessedPage:
        title = i18n("Image saved");
        text = i18n("The current page's processed scanned image has been saved to <kbd>%1</kbd>",
                    fileName);
        break;
    case Scandoc::SaveUnprocessedPage:
        title = i18n("Image saved");
        text = i18n("The current page's unprocessed scanned image has been saved to <kbd>%1</kbd>",
                    fileName);
        break;
    }

    QMessageBox::information(this, title, text);
}

bool ScannedPagesWidget::saveFile(const QString &fileName)
{
    if (m_saveMode == Scandoc::SavePdf) {
        // We want to save PDF output and have to do some processing

        QVector<QString> in;
        for (const QString &basename : m_pages) {
            in.append(basename + s_jpg);
        }

        Q_EMIT showMessage(i18n("Saving all scanned pages as PDF"));
        ProcessHelper generatePdf(m_settings->savePdfCommand());
        connect(&generatePdf, &ProcessHelper::errorMessage,
                this, &ScannedPagesWidget::processErrorMessage);
        generatePdf.addValue(s_inPlaceholder, in);
        generatePdf.addValue(s_outPlaceholder, fileName);
        const auto pdfGenerated = generatePdf.run();

        if (pdfGenerated && m_encryptPdfButton->isChecked()) {
            // We want to encrypt the PDF output

            Q_EMIT showMessage(i18n("Encrypting the generated PDF file"));

            const auto password = PasswordDialog::getPassword(this);
            if (password.isEmpty()) {
                // If we don't get a password, the dialog has been rejected.
                // We can't encrypt in this case.

                QFile::remove(fileName);
                QTimer::singleShot(0, this, [this]
                {
                    QMessageBox::warning(this,
                        i18n("PDF encryption failed"),
                        i18n("<p>No password has been provided. Can't encrypt the generated PDF "
                             "file!</p>"
                             "<p>The unencrypted PDF output has been deleted.</p>"));
                });
                return false;

            } else {
                // Do the encryption

                const auto encryptedFileName = m_tmpDir->filePath(QStringLiteral("encrypted.pdf"));

                ProcessHelper encryptPdf(m_settings->encryptPdfCommand());
                connect(&encryptPdf, &ProcessHelper::errorMessage,
                        this, &ScannedPagesWidget::processErrorMessage);
                encryptPdf.addValue(s_passwordPlaceholder, password);
                encryptPdf.addValue(s_inPlaceholder, fileName);
                encryptPdf.addValue(s_outPlaceholder, encryptedFileName);

                if (encryptPdf.run()) {
                    // The encryption command call succeeded, so move the encrypted version of
                    // the PDF to the original PDF output's place (we have to delete it and rename
                    // the encrypted file to achieve this)

                    if (! QFile::remove(fileName)
                        || ! QFile::rename(encryptedFileName, fileName)) {

                        QTimer::singleShot(0, this, [this, encryptedFileName, fileName]
                        {
                            QMessageBox::warning(this,
                                i18n("PDF encryption failed"),
                                i18n("<p>Could not move the encrypted PDF file <kbd>%1</kbd> to "
                                     "<kbd>%2</kbd>.</p>"
                                     "<p>This should not happen. Maybe the file's permissions "
                                     "changed while the encryption process was running?</p>",
                                     encryptedFileName, fileName));
                        });

                        return false;

                    } else {
                        // Moving the encrypted file succeeded
                        m_notSaved.clear();
                        return true;
                    }

                } else {
                    // The encryption command call failed
                    return false;
                }
            }

        } else {
            // We don't want to encrypt the PDF file
            if (pdfGenerated) {
                m_notSaved.clear();
            }
            return pdfGenerated;
        }

    } else {
        // Otherwise, we simply want to copy an existing file elsewhere

        // Check if we can write the file. If it exists, it will be deleted, but the user has been
        // asked by the QFileDialog if this is okay. And yes, we have to do it in this way, because
        // QFileInfo::isWritable does not work as expected on Windows (returns false for
        // "My Documents").
        QFile test(fileName);
        if (test.open(QIODevice::WriteOnly)) {
            test.close();
            test.remove();
        } else {
            QMessageBox::warning(this, i18n("Writing error"),
                i18n("File <kbd>%1</kbd> can't be written! Please check the respective permissions!",
                    QDir::toNativeSeparators(fileName)));
            return false;
        }

        // Copy the file

        const auto &basename = m_pages.at(m_currentPageIndex);
        bool success = false;

        if (m_saveMode == Scandoc::SaveProcessedPage) {
            Q_EMIT showMessage(i18n("Saving current page's processed scanned image"));
            success = QFile::copy(basename + s_jpg, fileName);
        } else if (m_saveMode == Scandoc::SaveUnprocessedPage) {
            Q_EMIT showMessage(i18n("Saving current page's unprocessed scanned image"));
            success = QFile::copy(basename + s_png, fileName);
        } else {
            // This should not happen
            Q_ASSERT_X(false, "saveFile", "Invalid save mode!");
            success = false;
        }

        if (success) {
            m_notSaved.removeOne(basename);
        }

        return success;
    }
}

void ScannedPagesWidget::sendPdf()
{
    const auto pdfFile = m_tmpDir->filePath(m_settings->outputFileName(
        nextFileNumber(m_tmpDir->path(), s_pdf, true), true)).append(s_pdf);
    if (! saveFile(pdfFile)) {
        return;
    }

    ProcessHelper processHelper(m_settings->sendViaMailCommand());
    connect(&processHelper, &ProcessHelper::errorMessage,
            this, &ScannedPagesWidget::processErrorMessage);
    processHelper.addValue(s_pdfPlaceholder, pdfFile);
    const bool success = processHelper.run();

    if (success && m_settings->resetAfterSaving()) {
        deleteAllPages(false);
    }
}

void ScannedPagesWidget::processErrorMessage(const QString &message)
{
    // We do this via a QTimer::singleShot so that the BusyHelper created
    // inside the ProcessHelper is destroyed before the QMessageBox::warning
    // call happens and we already have the default cursor back
    QTimer::singleShot(0, this, [this, message]
    {
        QMessageBox::warning(this,
            i18n("Command execution failed"),
            i18n("<p>The execution of a helper program failed. The error message was:</p>"
                 "<p>%1</p>", message));
    });
}

void ScannedPagesWidget::updateScanProgress(int progress)
{
    m_progressBar->setValue(progress);

    if (progress > 0) {
        m_saneInterface->lockScanImage();
        const auto *scanImage = m_saneInterface->scanImage();
        if (! scanImage->isNull()) {
            const auto scaledImage = scanImage->scaled(m_previewImage->size(), Qt::KeepAspectRatio);
            m_previewImage->setPixmap(QPixmap::fromImage(scaledImage));
        }
        m_saneInterface->unlockScanImage();
    }
}

void ScannedPagesWidget::duplexEnabled(bool state)
{
    if (state) {
        m_scanButton->setMenu(m_duplexMenu);
        m_scanButton->setPopupMode(QToolButton::MenuButtonPopup);
    } else {
        m_scanButton->setMenu(nullptr);
        m_scanButton->setPopupMode(QToolButton::DelayedPopup);
        updateScanMode(Scandoc::DefaultScanMode);
    }
}

void ScannedPagesWidget::updateScanMode(Scandoc::ScanMode mode)
{
    // Sometimes, it takes a while until the new setting is saved ...
    BusyHelper busyHelper;

    Q_EMIT scanModeChanged(mode);
    m_scanMode = mode;
    switch (mode) {
    case Scandoc::DefaultScanMode:
        m_scanButton->setText(i18n("Scan page"));
        break;
    case Scandoc::OneSidedScanMode:
        m_scanButton->setText(i18n("Scan page (one-sided)"));
        break;
    case Scandoc::DuplexScanMode:
        m_scanButton->setText(i18n("Scan page (duplex)"));
        break;
    };
}

void ScannedPagesWidget::updateSaveMode(Scandoc::SaveMode mode)
{
    m_saveMode = mode;
    switch (mode) {
    case Scandoc::SavePdf:
        m_saveButton->setText(i18n("Save PDF"));
        break;
    case Scandoc::SaveProcessedPage:
        m_saveButton->setText(i18n("Save processed page"));
        break;
    case Scandoc::SaveUnprocessedPage:
        m_saveButton->setText(i18n("Save unprocessed page"));
        break;
    }
}

void ScannedPagesWidget::saveFiles(Scandoc::SaveMode mode)
{
    QString title;
    QString extension;
    QString message;
    QString success;
    switch (mode) {
    case Scandoc::SavePdf:
        Q_ASSERT_X(false, "saveFiles", "Invalid save mode!");
        return;
        break;
    case Scandoc::SaveProcessedPage:
        extension = s_jpg;
        title = i18n("Please select a folder for the processed scanned JPG files");
        message = i18n("Saving all processed scanned JPG files");
        success = i18n("All processed scanned JPG files have been saved!");
        break;
    case Scandoc::SaveUnprocessedPage:
        extension = s_png;
        title = i18n("Please select a folder for the unprocessed scanned PNG files");
        message = i18n("Saving all unprocessed scanned PNG files");
        success = i18n("All unprocessed scanned PNG files have been saved!");
        break;
    }

    auto targetDir = QFileDialog::getExistingDirectory(this, title, m_settings->saveTargetFolder());
    if (targetDir.isEmpty()) {
        return;
    }
    targetDir.append(QLatin1Char('/'));

    // Create and remove a test file to see if we can write to the select target dir

    // Find a save, non-existant file name
    int testNumber = 0;
    QString testFile;
    while (true) {
        testFile = targetDir + QStringLiteral("scandoc_test.%1").arg(testNumber);
        if (! QFile::exists(testFile)) {
            break;
        }
        testNumber++;
    }

    // Do the actual write test
    QFile test(testFile);
    if (test.open(QIODevice::WriteOnly)) {
        test.close();
        test.remove();
    } else {
        QMessageBox::warning(this, i18n("Writing error"),
            i18n("Can't create a file in <kbd>%1</kbd>! Please check the respective "
                 "permissions!", QDir::toNativeSeparators(targetDir)));
        return;
    }

    // Check if we're using an empty directory

    QDir empyTest(targetDir);
    if (! empyTest.isEmpty()
        && QMessageBox::question(this, i18n("Target folder is not empty"),
               i18n("<p>The selected target folder <kbd>%1</kbd> is not empty. There could be "
                    "collisions with existing files.</p>"
                    "<p>Do you really want to use this directory?</p>",
                    QDir::toNativeSeparators(targetDir)),
               QMessageBox::Yes | QMessageBox::Cancel,
               QMessageBox::Cancel)
           != QMessageBox::Yes) {

        return;
    }

    Q_EMIT showMessage(message);
    BusyHelper busyHelper;

    // Generate a list of all files we want to copy
    QVector<QString> in;
    for (const QString &basename : m_pages) {
        in.append(basename + extension);
    }

    const auto numberLength = QString::number(in.size()).size();

    for (int i = 0; i < in.size(); i++) {
        const auto source = in.at(i);
        const auto target = QStringLiteral("%1%2%3").arg(
            targetDir,
            QString::number(i + 1).rightJustified(numberLength, QLatin1Char('0')),
            extension);

        if (QFile::exists(target)) {
            QTimer::singleShot(0, this, [this, i, source, target]
            {
                QMessageBox::warning(this, i18n("Writing error"),
                    i18n("<p>Copying <kbd>%1</kbd> to <kbd>%2</kbd> rejected: The target file name "
                         "already exists!</p>"
                         "<p>Saving all scanned images failed!</p>",
                         QDir::toNativeSeparators(source), QDir::toNativeSeparators(target)));
            });
            return;
        }

        if (! QFile::copy(source, target)) {
            QTimer::singleShot(0, this, [this, i, source, target]
            {
                QMessageBox::warning(this, i18n("Writing error"),
                    i18n("<p>Copying <kbd>%1</kbd> to <kbd>%2</kbd> failed! Please check the "
                         "respective permissions!</p>"
                         "<p>Saving all scanned images failed!</p>",
                         QDir::toNativeSeparators(source), QDir::toNativeSeparators(target)));
            });
            return;

        } else {
            m_notSaved.removeOne(m_pages.at(i));
        }
    }

    // If we reach here, all images have been copied.
    if (m_settings->resetAfterSaving()) {
        deleteAllPages(false);
    }

    QTimer::singleShot(0, this, [this, success]
    {
        QMessageBox::information(this, i18n("Images saved"), success);
    });
}

bool ScannedPagesWidget::allPagesSaved() const
{
    return m_notSaved.isEmpty();
}
