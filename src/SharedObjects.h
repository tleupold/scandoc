// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef SHAREDOBJECTS_H
#define SHAREDOBJECTS_H

// Qt includes
#include <QObject>

// Local classes
class Settings;

// libksane classes
namespace KSaneCore
{
class Interface;
}

class SharedObjects : public QObject
{
    Q_OBJECT

public:
    explicit SharedObjects(QObject *parent = nullptr);
    Settings *settings() const;
    KSaneCore::Interface *saneInterface() const;

private: // Variables
    Settings *m_settings;
    KSaneCore::Interface *m_saneInterface;

};

#endif // SHAREDOBJECTS_H
