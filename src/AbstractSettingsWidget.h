// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef ABSTRACTSETTINGSWIDGET_H
#define ABSTRACTSETTINGSWIDGET_H

// Qt includes
#include <QWidget>

// Local classes
class SharedObjects;
class Settings;

// Qt classes
class QVBoxLayout;
class QPushButton;
class QScrollArea;

class AbstractSettingsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AbstractSettingsWidget(SharedObjects *sharedObjects, QWidget *parent = nullptr);

Q_SIGNALS:
    void showMessage(const QString &message);

protected: // Functions
    Settings *settings() const;
    QVBoxLayout *mainLayout() const;
    QVBoxLayout *pageLayout() const;
    QScrollArea *scrollArea() const;
    void addApplyButton();
    QPushButton *applyButton() const;
    virtual void saveSettings() = 0;

private Q_SLOTS:
    void applyChanges();

private: // Variables
    Settings *m_settings;
    QVBoxLayout *m_mainLayout;
    QVBoxLayout *m_pageLayout;
    QScrollArea *m_scrollArea;
    QPushButton *m_applyButton;

};

#endif // ABSTRACTSETTINGSWIDGET_H
