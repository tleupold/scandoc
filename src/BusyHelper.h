// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef BUSYHELPER_H
#define BUSYHELPER_H

// Qt includes
#include <QObject>

class BusyHelper : public QObject
{
    Q_OBJECT

public:
    explicit BusyHelper(QObject *parent = nullptr);
    ~BusyHelper();

private: // Variables
    QWidget *m_mainWindow;

};

#endif // BUSYHELPER_H
