// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef SCANNEDPAGESWIDGET_H
#define SCANNEDPAGESWIDGET_H

// Local includes
#include "Scandoc.h"

// KDE includes
#include <KSaneCore/Interface>

// Qt includes
#include <QWidget>
#include <QImage>
#include <QTransform>

// Local classes
class SharedObjects;
class Settings;
class PagePreview;

// Qt classes
class QPushButton;
class QTemporaryDir;
class QToolButton;
class QLabel;
class QStackedLayout;
class QProgressBar;
class QMenu;

class ScannedPagesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ScannedPagesWidget(SharedObjects *sharedObjects, QWidget *parent = nullptr);
    ~ScannedPagesWidget();
    bool allPagesSaved() const;

Q_SIGNALS:
    void showMessage(const QString &message);
    void scanning(bool status);
    void scanModeChanged(Scandoc::ScanMode mode);
    void requestStartScan(Scandoc::ScanMode mode);

public Q_SLOTS:
    void duplexEnabled(bool state);
    void updateScanMode(Scandoc::ScanMode mode);

private Q_SLOTS:
    void startScan();
    void cancelScan();
    void scanFinished(KSaneCore::Interface::ScanStatus, const QString &);
    void pageScanned(const QImage &scannedImage);
    void showPage(int index);
    void rotatePage();
    void deletePage();
    void deleteAllPages(bool ask);
    void kSaneMessage(KSaneCore::Interface::ScanStatus status, const QString &message);
    void getSaveFileName();
    bool saveFile(const QString &fileName);
    void sendPdf();
    void processErrorMessage(const QString &message);
    void batchModeCountDown(int seconds);
    void updateScanProgress(int progress);
    void updateSaveMode(Scandoc::SaveMode mode);
    void saveFiles(Scandoc::SaveMode mode);

private: // Functions
    void updatePageNavigation();
    void movePage(int target);
    bool postProcess(const QString &in, const QString &out);
    void updateScanLabel();
    int nextFileNumber(const QString &path, const QString &extension,
                       bool forceNumber = false) const;

private: // Variables
    Settings *m_settings;
    KSaneCore::Interface *m_saneInterface;
    QTemporaryDir *m_tmpDir;
    QStackedLayout *m_stackedLayout;
    QVector<QString> m_pages;
    int m_pagesCount = 0;
    int m_scanSeriesCount = 0;
    int m_currentPageIndex = -1;
    Scandoc::ScanMode m_scanMode = Scandoc::DefaultScanMode;
    QTransform m_rotate;
    Scandoc::SaveMode m_saveMode = Scandoc::SavePdf;
    QVector<QString> m_notSaved;

    QWidget *m_scanWidget;
    QLabel *m_scanLabel;
    QProgressBar *m_progressBar;
    QLabel *m_previewImage;

    QWidget *m_displayWidget;
    QLabel *m_pageLabel;
    QToolButton *m_scanButton;
    QMenu *m_duplexMenu;
    QToolButton *m_previousPageButton;
    QToolButton *m_nextPageButton;
    QToolButton *m_deleteButton;
    QToolButton *m_deleteAllButton;
    QToolButton *m_moveBackwardsButton;
    QToolButton *m_moveForthButton;
    QToolButton *m_rotateButton;
    PagePreview *m_pagePreview;
    QToolButton *m_saveButton;
    QPushButton *m_sendPdfButton;
    QPushButton *m_encryptPdfButton;

};

#endif // SCANNEDPAGESWIDGET_H
