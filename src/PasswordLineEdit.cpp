// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "PasswordLineEdit.h"

// Qt includes
#include <QAction>

PasswordLineEdit::PasswordLineEdit(QWidget *parent) : QLineEdit(parent)
{
    m_show = new QAction(this);
    m_show->setIcon(QIcon::fromTheme(QStringLiteral("view-visible")));
    connect(m_show, &QAction::triggered, this, &PasswordLineEdit::showPassword);

    m_hide = new QAction(this);
    m_hide->setIcon(QIcon::fromTheme(QStringLiteral("view-hidden")));
    connect(m_hide, &QAction::triggered, this, &PasswordLineEdit::hidePassword);

    hidePassword();
}

void PasswordLineEdit::showPassword()
{
    removeAction(m_show);
    addAction(m_hide, QLineEdit::TrailingPosition);
    setEchoMode(QLineEdit::Normal);
}

void PasswordLineEdit::hidePassword()
{
    removeAction(m_hide);
    addAction(m_show, QLineEdit::TrailingPosition);
    setEchoMode(QLineEdit::Password);
}
