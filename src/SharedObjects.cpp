// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "SharedObjects.h"
#include "Settings.h"

// KDE includes
#include <KSaneCore/Interface>

SharedObjects::SharedObjects(QObject *parent) : QObject(parent)
{
    m_settings = new Settings(this);
    m_saneInterface = new KSaneCore::Interface(this);
}

Settings *SharedObjects::settings() const
{
    return m_settings;
}

KSaneCore::Interface *SharedObjects::saneInterface() const
{
    return m_saneInterface;
}
