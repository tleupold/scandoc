// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "MainWindow.h"
#include "SharedObjects.h"
#include "Settings.h"
#include "ScannerSettingsWidget.h"
#include "HelperProgramsWidget.h"
#include "ScannedPagesWidget.h"
#include "SavingWidget.h"
#include "version.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QDebug>
#include <QVBoxLayout>
#include <QCloseEvent>
#include <QApplication>
#include <QTabWidget>
#include <QStatusBar>
#include <QCloseEvent>
#include <QMessageBox>

MainWindow::MainWindow(SharedObjects *sharedObjects)
    : QMainWindow(),
      m_settings(sharedObjects->settings())
{
    setWindowTitle(i18n("Scandoc %1", QStringLiteral(VERSION_STRING)));
    setWindowIcon(QIcon::fromTheme(QStringLiteral("scandoc")));

    auto *mainWidget = new QWidget;
    auto *layout = new QVBoxLayout(mainWidget);
    setCentralWidget(mainWidget);

    m_statusBar = statusBar();

    m_tabWidget = new QTabWidget;
    layout->addWidget(m_tabWidget);

    // Scanned pages
    m_scannedPagesWidget = new ScannedPagesWidget(sharedObjects);
    m_tabWidget->addTab(m_scannedPagesWidget, i18n("Scanned pages"));
    connect(m_scannedPagesWidget, &ScannedPagesWidget::showMessage, this, &MainWindow::showMessage);
    m_scannedPagesWidget->setEnabled(false);

    // Scanner settings
    m_scannerSettingsWidget = new ScannerSettingsWidget(sharedObjects);
    connect(m_scannerSettingsWidget, &ScannerSettingsWidget::scannerOpened,
            m_scannedPagesWidget, &QWidget::setEnabled);
    connect(m_scannedPagesWidget, &ScannedPagesWidget::requestStartScan,
            m_scannerSettingsWidget, &ScannerSettingsWidget::startScan);
    m_tabWidget->addTab(m_scannerSettingsWidget, i18n("Scanner settings"));
    connect(m_scannerSettingsWidget, &ScannerSettingsWidget::showMe,
            this, [this]
            {
                m_tabWidget->setCurrentIndex(m_tabWidget->indexOf(m_scannerSettingsWidget));
            });
    connect(m_scannerSettingsWidget, &ScannerSettingsWidget::showMessage,
            this, &MainWindow::showMessage);
    connect(m_scannerSettingsWidget, &ScannerSettingsWidget::duplexEnabled,
            m_scannedPagesWidget, &ScannedPagesWidget::duplexEnabled);
    connect(m_scannedPagesWidget, &ScannedPagesWidget::scanModeChanged,
            m_scannerSettingsWidget, &ScannerSettingsWidget::scanModeChanged);
    connect(m_scannerSettingsWidget, &ScannerSettingsWidget::updateScanMode,
            m_scannedPagesWidget, &ScannedPagesWidget::updateScanMode);

    // Helper programs
    m_helperProgramsWidget = new HelperProgramsWidget(sharedObjects);
    m_tabWidget->addTab(m_helperProgramsWidget, i18n("Helper programs"));
    connect(m_helperProgramsWidget, &AbstractSettingsWidget::showMessage,
            this, &MainWindow::showMessage);

    // Saving settings
    m_savingWidget = new SavingWidget(sharedObjects);
    m_tabWidget->addTab(m_savingWidget, i18n("Saving"));
    connect(m_savingWidget, &AbstractSettingsWidget::showMessage, this, &MainWindow::showMessage);

    // Disable all settings tabs whilst scanning

    connect(m_scannedPagesWidget, &ScannedPagesWidget::scanning,
            this, [this](bool state)
            {
                m_tabWidget->setTabEnabled(m_tabWidget->indexOf(m_scannerSettingsWidget), ! state);
                m_tabWidget->setTabEnabled(m_tabWidget->indexOf(m_helperProgramsWidget), ! state);
                m_tabWidget->setTabEnabled(m_tabWidget->indexOf(m_savingWidget), ! state);
            });

    // Restore or set the MainWindow geometry
    if (! restoreGeometry(m_settings->mainWindowGeometry())) {
        resize(QSize(600, 750));
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (! m_scannedPagesWidget->allPagesSaved()
        && QMessageBox::question(this, i18n("Close Scandoc"),
               i18n("<p>At least one scanned page has not been saved yet!</p>"
                    "<p>Do you want to close the program anyway?</p>"),
               QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::No) {

        event->ignore();
        return;
    }

    m_settings->saveMainWindowGeometry(saveGeometry());
    QApplication::quit();
}

void MainWindow::showMessage(const QString &message)
{
    m_statusBar->showMessage(message, 3000);
}
