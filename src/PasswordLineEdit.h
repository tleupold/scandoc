// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef PASSWORDLINEEDIT_H
#define PASSWORDLINEEDIT_H

// Qt includes
#include <QLineEdit>

// Qt classes
class QAction;

class PasswordLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    explicit PasswordLineEdit(QWidget *parent = nullptr);

private Q_SLOTS:
    void showPassword();
    void hidePassword();

private: // Variables
    QAction *m_show;
    QAction *m_hide;

};

#endif // PASSWORDLINEEDIT_H
