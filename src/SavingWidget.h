// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef SAVINGWIDGET_H
#define SAVINGWIDGET_H

// Local includes
#include "AbstractSettingsWidget.h"

// Local classes
class SharedObjects;

// Qt classes
class QRadioButton;
class QCheckBox;
class QLabel;
class QLineEdit;

class SavingWidget : public AbstractSettingsWidget
{
    Q_OBJECT

public:
    explicit SavingWidget(SharedObjects *sharedObjects, QWidget *parent = nullptr);

private Q_SLOTS:
    void selectCustomFolder();
    void showExampleFilename();

private: // Functions
    void selectSaveTarget();
    QString fileNameTemplate() const;
    void saveSettings() override;

private: // Variables
    QRadioButton *m_documentsFolderTarget;
    QRadioButton *m_customFolderTarget;
    QRadioButton *m_lastUsedFolderTarget;
    QLabel *m_customFolder;
    QLineEdit *m_outputFileNameTemplate;
    QLabel *m_exampleFileName;
    QCheckBox *m_omitFirstNumber;
    QCheckBox *m_resetAfterSaving;

};

#endif // SAVINGWIDGET_H
