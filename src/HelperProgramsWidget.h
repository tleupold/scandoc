// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef HELPERPROGRAMSWIDGET_H
#define HELPERPROGRAMSWIDGET_H

// Local includes
#include "AbstractSettingsWidget.h"

// Local classes
class SharedObjects;

// Qt classes
class QLineEdit;
class QPushButton;

class HelperProgramsWidget : public AbstractSettingsWidget
{
    Q_OBJECT

public:
    explicit HelperProgramsWidget(SharedObjects *sharedObjects, QWidget *parent = nullptr);

private: // Functions
    void saveSettings() override;

private: // Variables
    QLineEdit *m_postProcessingCommand;
    QLineEdit *m_savePdfCommand;
    QLineEdit *m_encryptPdfCommand;
    QLineEdit *m_sendViaMailCommand;

};

#endif // HELPERPROGRAMSWIDGET_H
