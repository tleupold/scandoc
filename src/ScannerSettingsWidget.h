// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef SCANNERSETTINGSWIDGET_H
#define SCANNERSETTINGSWIDGET_H

// Local includes
#include "AbstractSettingsWidget.h"
#include "Scandoc.h"

// Local classes
class SharedObjects;
class Settings;

// KDE classes
namespace KSaneCore
{
class DeviceInformation;
class Option;
class Interface;
}

// Qt classes
class QComboBox;
class QScrollArea;
class QLabel;
class QGridLayout;
class QCheckBox;

class ScannerSettingsWidget : public AbstractSettingsWidget
{
    Q_OBJECT

public:
    explicit ScannerSettingsWidget(SharedObjects *sharedObjects, QWidget *parent = nullptr);
    void refresh();

public Q_SLOTS:
    void scanModeChanged(Scandoc::ScanMode mode);
    void startScan(Scandoc::ScanMode mode);

Q_SIGNALS:
    void scannerOpened(bool success);
    void showMe();
    void duplexEnabled(bool state);
    void updateScanMode(Scandoc::ScanMode mode);

private Q_SLOTS:
    void reloadDevicesList();
    void availableDevicesChanged(const QList<KSaneCore::DeviceInformation *> &deviceList);
    void openDevice(int index);
    void checkRestoreSource();

private: // Structs
    struct OptionComponents {
        KSaneCore::Option *option = nullptr;
        QLabel *label = nullptr;
        QWidget *control = nullptr;
    };

private: // Functions
    OptionComponents addNamedOption(const QString &name, QGridLayout *layout, int &row);
    OptionComponents getOptionComponents(KSaneCore::Option *option) const;
    void addOptionWidgets(QGridLayout *layout, OptionComponents &optionComponents, int &row);
    QString unitSuffix(KSaneCore::Option *option) const;
    QComboBox *createSourceComboBox() const;
    QString stringIsChecked(QCheckBox *checkBox) const;
    QString currentDeviceString() const;
    void saveSettings() override;

private: // Variables
    KSaneCore::Interface *m_saneInterface;
    QComboBox *m_devices;
    QVector<KSaneCore::Option *> m_handledOptions;
    bool m_restoreSource = false;
    int m_lastSource = 0;

    QCheckBox *m_hasDuplex;
    QComboBox *m_source = nullptr;
    QComboBox *m_oneSidedSource;
    QComboBox *m_duplexSource;
    QCheckBox *m_rotateEvenPages;

};

#endif // SCANNERSETTINGSWIDGET_H
