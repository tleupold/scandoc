// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef VALUESCOMBOBOX_H
#define VALUESCOMBOBOX_H

// Qt includes
#include <QComboBox>

class ValuesComboBox : public QComboBox
{
    Q_OBJECT

public:
    explicit ValuesComboBox(QWidget *parent = nullptr);

Q_SIGNALS:
    void currentValueChanged(QVariant value);

};

#endif // VALUESCOMBOBOX_H
