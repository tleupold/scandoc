// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "PasswordDialog.h"
#include "PasswordLineEdit.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QVBoxLayout>
#include <QGridLayout>
#include <QLabel>
#include <QDebug>
#include <QDialogButtonBox>
#include <QPushButton>

PasswordDialog::PasswordDialog(QWidget *parent) : QDialog(parent)
{
    auto *layout = new QVBoxLayout(this);
    layout->addWidget(new QLabel(i18n("Please provide a password for PDF encryption")));

    auto *passLayout = new QGridLayout;
    layout->addLayout(passLayout);

    passLayout->addWidget(new QLabel(i18n("Password:")), 0, 0);
    m_password = new PasswordLineEdit;
    m_password->setEchoMode(QLineEdit::Password);
    connect(m_password, &QLineEdit::textChanged, this, &PasswordDialog::checkPassword);
    passLayout->addWidget(m_password, 0, 1);

    passLayout->addWidget(new QLabel(i18n("Repeat password:")), 1, 0);
    m_repeatedPassword = new PasswordLineEdit;
    m_repeatedPassword->setEchoMode(QLineEdit::Password);
    connect(m_repeatedPassword, &QLineEdit::textChanged, this, &PasswordDialog::checkPassword);
    passLayout->addWidget(m_repeatedPassword, 1, 1);

    m_match = new QLabel;
    m_match->setAlignment(Qt::AlignRight);
    layout->addWidget(m_match);

    m_buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(m_buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
    layout->addWidget(m_buttonBox);

    checkPassword();
}

void PasswordDialog::checkPassword()
{
    const auto password = m_password->text();
    const auto repeatedPassword = m_repeatedPassword->text();

    if (password.isEmpty()) {
        m_match->setText(i18n("<i>Please enter a password</i>"));
    } else {
        if (repeatedPassword.isEmpty()) {
            m_match->setText(i18n("<i>Please enter the password again</i>"));
        } else {
            if (password == repeatedPassword) {
                m_match->setText(i18n("<span style=\"color: green\">The passwords match</span>"));
            } else {
                m_match->setText(i18n("<span style=\"color: red\">The passwords don't match"
                                      "</span>"));
            }
        }
    }

    m_buttonBox->button(QDialogButtonBox::Ok)->setEnabled(! password.isEmpty()
                                                          && (password == repeatedPassword));
}

QString PasswordDialog::password() const
{
    const auto password = m_password->text();
    const auto repeatedPassword = m_repeatedPassword->text();
    if (password.isEmpty() || (password != repeatedPassword)) {
        return QString();
    } else {
        return m_password->text();
    }
}

QString PasswordDialog::getPassword(QWidget *parent)
{
    PasswordDialog dialog(parent);
    if (dialog.exec()) {
        return dialog.password();
    } else {
        return QString();
    }
}
