// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef PROCESSHELPER_H
#define PROCESSHELPER_H

// Qt includes
#include <QObject>
#include <QStringList>
#include <QHash>

class ProcessHelper : public QObject
{
    Q_OBJECT

public:
    explicit ProcessHelper(const QString &command, QObject *parent = nullptr);
    void addValue(const QString &key, const QString &value);
    void addValue(const QString &key, const QVector<QString> &valueList);
    bool run();

Q_SIGNALS:
    void errorMessage(const QString &message);

private: // Variables
    const QString m_command;
    QHash<QString, QVector<QString>> m_values;
    bool m_failed = false;

};

#endif // PROCESSHELPER_H
