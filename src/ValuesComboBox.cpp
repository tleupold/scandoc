// SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "ValuesComboBox.h"

ValuesComboBox::ValuesComboBox(QWidget *parent) : QComboBox(parent)
{
    connect(this, &QComboBox::currentIndexChanged,
            this, [this](int index)
            {
                Q_EMIT currentValueChanged(itemData(index));
            });
}
