// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef SETTINGS_H
#define SETTINGS_H

// Local includes
#include "Scandoc.h"

// KDE includes
#include <KSharedConfig>

class Settings : public QObject
{
    Q_OBJECT

public:
    enum SaveTarget {
        TargetDocumentsFolder,
        TargetCustomFolder,
        TargetLastUsedFolderTarget
    };

    explicit Settings(QObject *parent);

    // Main

    void saveMainWindowGeometry(const QByteArray &data);
    QByteArray mainWindowGeometry() const;

    // Scanner settings

    void saveScannerSettings(const QString &name, const QMap<QString, QString> &settings);
    QMap<QString, QString> scannerSettings(const QString &name) const;

    void saveScandocSettings(const QString &name, const QMap<QString, QString> &settings);
    QMap<QString, QString> scandocSettings(const QString &name) const;

    void saveLastScanMode(const QString &name, Scandoc::ScanMode mode);
    Scandoc::ScanMode lastScanMode(const QString &name);

    void setRotateEvenPages(bool state);
    bool rotateEvenPages() const;

    // Commands

    void saveCommandsRevision();
    int commandsRevision() const;

    void savePostProcessingCommand(const QString &command);
    QString postProcessingCommand() const;

    void saveSavePdfCommand(const QString &command);
    QString savePdfCommand() const;

    void saveSendViaMailCommand(const QString &command);
    QString sendViaMailCommand() const;

    void saveEncryptPdfCommand(const QString &command);
    QString encryptPdfCommand() const;

    // Saving settings

    void saveDefaultSaveTarget(Settings::SaveTarget target);
    Settings::SaveTarget defaultSaveTarget() const;

    void saveDefaultTargetFolder(const QString &path);
    QString defaultTargetFolder() const;

    QString saveTargetFolder() const;

    void saveOutputFileNameTemplate(const QString &name);
    QString outputFileNameTemplate() const;

    QString outputFileNameWildcard(bool forceNumber = false) const;
    QString outputFileName(int number, bool forceNumber = false) const;
    QString outputFileName(QString fileNameTemplate, int number, bool forceNumber = false) const;

    void saveOmitFirstNumber(bool state);
    bool omitFirstNumber() const;

    void saveResetAfterSaving(bool state);
    bool resetAfterSaving() const;

private: // Functions
    QString finishOutputFileName(QString &fileNameTemplate) const;

private: // Variables
    KSharedConfig::Ptr m_config;
    bool m_rotateEvenPages = false;

};

#endif // SETTINGS_H
