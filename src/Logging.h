// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef LOG_H
#define LOG_H

// Qt includes
#include <QLoggingCategory>

Q_DECLARE_LOGGING_CATEGORY(scandocLog)

#endif // LOG_H
