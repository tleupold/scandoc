// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "ProcessHelper.h"
#include "BusyHelper.h"
#include "Logging.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QProcess>
#include <QDebug>
#include <QTimer>

static QChar s_space = QChar::fromLatin1(' ');

ProcessHelper::ProcessHelper(const QString &command, QObject *parent)
    : QObject(parent),
      m_command(command)
{
}

void ProcessHelper::addValue(const QString &key, const QString &value)
{
    m_values[key] = { value };
}

void ProcessHelper::addValue(const QString &key, const QVector<QString> &valueList)
{
    m_values[key] = valueList;
}

bool ProcessHelper::run()
{
    BusyHelper busyHelper;

    const auto keys = m_values.keys();
    const auto commandParts = m_command.split(s_space);
    const auto program = commandParts.first();

    QStringList arguments;
    for (int i = 1; i < commandParts.count(); i++) {
        const auto &argument = commandParts.at(i);

        if (keys.contains(argument)) {
            for (const QString &value : m_values[argument]) {
                arguments.append(value);
            }
        } else {
            arguments.append(argument);
        }
    }

    QProcess process;
    connect(&process, &QProcess::errorOccurred,
            this, [this]
            {
                m_failed = true;
            });

    qCDebug(scandocLog) << "Running command:" << program << "with the arguments:" << arguments;

    process.start(program, arguments);
    process.waitForFinished();

    if (m_failed) {
        Q_EMIT errorMessage(i18n("Could not execute \"%1\"", program));
        return false;
    }

    if (process.exitCode() != 0) {
        Q_EMIT errorMessage(QString::fromLocal8Bit(process.readAllStandardError()));
        return false;
    }

    return true;
}
