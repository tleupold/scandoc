// SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef PASSWORDDIALOG_H
#define PASSWORDDIALOG_H

// Qt includes
#include <QDialog>

// Local classes
class PasswordLineEdit;

// Qt classes
class QLabel;
class QDialogButtonBox;

class PasswordDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PasswordDialog(QWidget *parent = nullptr);
    QString password() const;
    static QString getPassword(QWidget *parent = nullptr);

private Q_SLOTS:
    void checkPassword();

private: // Variables
    PasswordLineEdit *m_password;
    PasswordLineEdit *m_repeatedPassword;
    QLabel *m_match;
    QDialogButtonBox *m_buttonBox;

};

#endif // PASSWORDDIALOG_H
