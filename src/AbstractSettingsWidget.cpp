// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "AbstractSettingsWidget.h"
#include "SharedObjects.h"
#include "Settings.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QVBoxLayout>
#include <QScrollArea>
#include <QPushButton>
#include <QHBoxLayout>

AbstractSettingsWidget::AbstractSettingsWidget(SharedObjects *sharedObjects, QWidget *parent)
    : QWidget(parent),
      m_settings(sharedObjects->settings())
{
    m_mainLayout = new QVBoxLayout(this);
    m_scrollArea = new QScrollArea;
    m_scrollArea->setWidgetResizable(true);
    auto *scrollWidget = new QWidget;
    m_pageLayout = new QVBoxLayout(scrollWidget);
    m_scrollArea->setWidget(scrollWidget);
    m_mainLayout->addWidget(m_scrollArea);
}

Settings *AbstractSettingsWidget::settings() const
{
    return m_settings;
}

QVBoxLayout *AbstractSettingsWidget::pageLayout() const
{
    return m_pageLayout;
}

QVBoxLayout *AbstractSettingsWidget::mainLayout() const
{
    return m_mainLayout;
}

QScrollArea *AbstractSettingsWidget::scrollArea() const
{
    return m_scrollArea;
}

void AbstractSettingsWidget::addApplyButton()
{
    m_applyButton = new QPushButton(i18n("Apply"));
    m_applyButton->setIcon(QIcon::fromTheme(QStringLiteral("dialog-ok-apply")));
    connect(m_applyButton, &QPushButton::clicked, this, &AbstractSettingsWidget::applyChanges);
    auto *applyLayout = new QHBoxLayout;
    m_mainLayout->addLayout(applyLayout);
    applyLayout->addStretch();
    applyLayout->addWidget(m_applyButton);
    m_applyButton->setEnabled(false);
}

QPushButton *AbstractSettingsWidget::applyButton() const
{
    return m_applyButton;
}

void AbstractSettingsWidget::applyChanges()
{
    m_applyButton->setEnabled(false);
    saveSettings();
    Q_EMIT showMessage(i18n("Settings saved!"));
}
