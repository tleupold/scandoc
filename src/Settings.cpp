// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "Settings.h"

// KDE includes
#include <KConfigGroup>

// Qt includes
#include <QDebug>
#include <QStandardPaths>
#include <QLocale>
#include <QDateTime>
#include <QRegularExpression>
#include <QHash>

// Note to self: Regular expressions are fun :-D
// This matches a string with exactly one "%" at the beginning and the end, containing at least
// one "n" and possibly other characters.
// This is achieved by the negative lookbehind and lookahead statements "(?<!%)" and "(?!%)":
// "(?<!%)%(?!%)" matches a "%" not preceded and not followed by another "%".
static const QRegularExpression s_matchNumberPlaceholder(QStringLiteral(
    "(?<!%)%(?!%)(.*?)(n+)(.*?)(?<!%)%(?!%)"));
static const QRegularExpression s_matchSinglePercent(QStringLiteral("(?<!%)%(?!%)"));

Settings::Settings(QObject *parent) : QObject(parent)
{
    m_config = KSharedConfig::openConfig();
}

// Main

static const QLatin1String s_main("main");
static const QLatin1String s_windowGeometry("windowGeometry");

void Settings::saveMainWindowGeometry(const QByteArray &data)
{
    auto group = m_config->group(s_main);
    group.writeEntry(s_windowGeometry, data);
    group.sync();
}

QByteArray Settings::mainWindowGeometry() const
{
    const auto group = m_config->group(s_main);
    return group.readEntry(s_windowGeometry, QByteArray());
}

// Scanner settings

static const QLatin1String s_scannerSettings("Settings for %1");

void Settings::saveScannerSettings(const QString &name, const QMap<QString, QString> &settings)
{
    auto group = m_config->group(s_scannerSettings.arg(name));
    group.deleteGroup();
    QMap<QString, QString>::const_iterator it = settings.constBegin();
    while (it != settings.constEnd()) {
        group.writeEntry(it.key(), it.value());
        it++;
    }
    group.sync();
}

QMap<QString, QString> Settings::scannerSettings(const QString &name) const
{
    QMap<QString, QString> settings;
    const auto group = m_config->group(s_scannerSettings.arg(name));
    const auto keys = group.keyList();
    for (const auto &key : keys) {
        settings.insert(key, group.readEntry(key));
    }
    return settings;
}

static const QLatin1String s_scandocSettings("Scandoc settings for %1");

void Settings::saveScandocSettings(const QString &name, const QMap<QString, QString> &settings)
{
    auto group = m_config->group(s_scandocSettings.arg(name));
    group.deleteGroup();
    QMap<QString, QString>::const_iterator it = settings.constBegin();
    while (it != settings.constEnd()) {
        group.writeEntry(it.key(), it.value());
        it++;
    }
    group.sync();
}

QMap<QString, QString> Settings::scandocSettings(const QString &name) const
{
    QMap<QString, QString> settings;
    const auto group = m_config->group(s_scandocSettings.arg(name));
    const auto keys = group.keyList();
    for (const auto &key : keys) {
        settings.insert(key, group.readEntry(key));
    }
    return settings;
}

static const QString s_lastScanMode = QStringLiteral("lastScanMode");

static const QHash<Scandoc::ScanMode, QString> s_scanModeToString {
    { Scandoc::DefaultScanMode,  QStringLiteral("DefaultScanMode") },
    { Scandoc::OneSidedScanMode, QStringLiteral("OneSidedScanMode") },
    { Scandoc::DuplexScanMode,   QStringLiteral("DuplexScanMode") }
};

static const QHash<QString, Scandoc::ScanMode> s_scanModeFromString {
    { QStringLiteral("DefaultScanMode"),  Scandoc::DefaultScanMode },
    { QStringLiteral("OneSidedScanMode"), Scandoc::OneSidedScanMode },
    { QStringLiteral("DuplexScanMode"),   Scandoc::DuplexScanMode }
};

void Settings::saveLastScanMode(const QString &name, Scandoc::ScanMode mode)
{
    auto group = m_config->group(s_scandocSettings.arg(name));
    group.writeEntry(s_lastScanMode, s_scanModeToString.value(mode));
    group.sync();
}

Scandoc::ScanMode Settings::lastScanMode(const QString &name)
{
    const auto group = m_config->group(s_scandocSettings.arg(name));
    const auto mode = group.readEntry(s_lastScanMode, QString());
    if (s_scanModeFromString.contains(mode)) {
        return s_scanModeFromString.value(mode);
    } else {
        return Scandoc::DefaultScanMode;
    }
}

void Settings::setRotateEvenPages(bool state)
{
    m_rotateEvenPages = state;
}

bool Settings::rotateEvenPages() const
{
    return m_rotateEvenPages;
}

// Commands

static constexpr int s_commandsRevision = 2;

static const QLatin1String s_commands("commands");
static const QLatin1String s_commandsRevisionKey("revision");
static const QLatin1String s_postProcessingCommand("postProcessingCommand");
static const QLatin1String s_savePdfCommand("savePdfCommand");
static const QLatin1String s_sendViaMailCommand("sendViaMailCommand");
static const QLatin1String s_encryptPdfCommand("encryptPdfCommand");

void Settings::saveCommandsRevision()
{
    auto group = m_config->group(s_commands);
    group.writeEntry(s_commandsRevisionKey, s_commandsRevision);
    group.sync();
}

int Settings::commandsRevision() const
{
    const auto group = m_config->group(s_commands);
    return group.readEntry(s_commandsRevisionKey, 1);
}

void Settings::savePostProcessingCommand(const QString &command)
{
    auto group = m_config->group(s_commands);
    group.writeEntry(s_postProcessingCommand, command);
    group.sync();
}

QString Settings::postProcessingCommand() const
{
    const auto group = m_config->group(s_commands);
    return group.readEntry(s_postProcessingCommand,
                           QStringLiteral("magick %IN% -level 15%,85% -format JPEG -quality 85 "
                                          "%OUT%"));
}

void Settings::saveSavePdfCommand(const QString &command)
{
    auto group = m_config->group(s_commands);
    group.writeEntry(s_savePdfCommand, command);
    group.sync();
}

QString Settings::savePdfCommand() const
{
    const auto group = m_config->group(s_commands);
    return group.readEntry(s_savePdfCommand,
                           QStringLiteral("pdfjam --a4paper %IN% --outfile %OUT%"));
}

void Settings::saveSendViaMailCommand(const QString &command)
{
    auto group = m_config->group(s_commands);
    group.writeEntry(s_sendViaMailCommand, command);
    group.sync();
}

QString Settings::sendViaMailCommand() const
{
    const auto group = m_config->group(s_commands);
    auto command = group.readEntry(s_sendViaMailCommand, QStringLiteral("kmail --attach %PDF%"));
    if (commandsRevision() == 1) {
        return command.replace(QLatin1String("%IN%"), QLatin1String("%PDF%"));
    } else {
        return command;
    }
}

void Settings::saveEncryptPdfCommand(const QString &command)
{
    auto group = m_config->group(s_commands);
    group.writeEntry(s_encryptPdfCommand, command);
    group.sync();
}

QString Settings::encryptPdfCommand() const
{
    const auto group = m_config->group(s_commands);
    return group.readEntry(s_encryptPdfCommand,
                           QStringLiteral("qpdf --encrypt %PASS% %PASS% 256 -- %IN% %OUT%"));
}

// Saving settings

static const QLatin1String s_saving("saving");
static const QLatin1String s_resetAfterSaving("resetAfterSaving");
static const QLatin1String s_defaultSaveTarget("defaultSaveTarget");
static const QHash<Settings::SaveTarget, QString> s_saveTargetToString {
    { Settings::TargetDocumentsFolder,      QStringLiteral("TargetDocumentsFolder") },
    { Settings::TargetCustomFolder,         QStringLiteral("TargetCustomFolder") },
    { Settings::TargetLastUsedFolderTarget, QStringLiteral("TargetLastUsedFolderTarget") }
};
static const QHash<QString, Settings::SaveTarget> s_saveTargetFromString {
    { QStringLiteral("TargetDocumentsFolder"),      Settings::TargetDocumentsFolder },
    { QStringLiteral("TargetCustomFolder"),         Settings::TargetCustomFolder },
    { QStringLiteral("TargetLastUsedFolderTarget"), Settings::TargetLastUsedFolderTarget }
};
static const QLatin1String s_defaultTargetFolder("defaultTargetFolder");
static const QLatin1String s_outputFileNameTemplate("outputFileNameTemplate");
static const QLatin1String s_omitFirstNumber("omitFirstNumber");

void Settings::saveDefaultSaveTarget(Settings::SaveTarget target)
{
    auto group = m_config->group(s_saving);
    group.writeEntry(s_defaultSaveTarget, s_saveTargetToString.value(target));
    group.sync();
}

Settings::SaveTarget Settings::defaultSaveTarget() const
{
    const auto group = m_config->group(s_saving);
    const auto savedSaveTarget = group.readEntry(s_defaultSaveTarget,
                                                 s_saveTargetToString.value(
                                                     Settings::TargetDocumentsFolder));
    return s_saveTargetFromString.contains(savedSaveTarget)
        ? s_saveTargetFromString.value(savedSaveTarget) : Settings::TargetDocumentsFolder;
}

void Settings::saveDefaultTargetFolder(const QString &path)
{
    auto group = m_config->group(s_saving);
    group.writeEntry(s_defaultTargetFolder, path);
    group.sync();
}

QString Settings::defaultTargetFolder() const
{
    const auto group = m_config->group(s_saving);
    return group.readEntry(s_defaultTargetFolder, QString());
}

QString Settings::saveTargetFolder() const
{
    const auto saveTarget = defaultSaveTarget();

    if (saveTarget == Settings::TargetDocumentsFolder) {
        QStringList locations;
        locations = QStandardPaths::standardLocations(QStandardPaths::DocumentsLocation);
        if (locations.isEmpty()) {
            locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
        }

        return locations.isEmpty() ? QString() : locations.first();

    } else {
        return defaultTargetFolder();
    }
}

void Settings::saveOutputFileNameTemplate(const QString &name)
{
    auto group = m_config->group(s_saving);
    group.writeEntry(s_outputFileNameTemplate, name);
    group.sync();
}

QString Settings::outputFileNameTemplate() const
{
    const auto group = m_config->group(s_saving);
    QString value = group.readEntry(s_outputFileNameTemplate, QStringLiteral("'scan'%_nn%"));
    return value.isEmpty() ? QStringLiteral("'scan'%_nn%") : value;
}

QString Settings::finishOutputFileName(QString &fileNameTemplate) const
{
    // Strip out single "%"
    fileNameTemplate.replace(s_matchSinglePercent, QString());

    // Replace "%%" with "%"
    fileNameTemplate.replace(QLatin1String("%%"), QLatin1String("%"));

    // Possibly insert date and time information
    return QLocale::system().toString(QDateTime::currentDateTime(), fileNameTemplate);
}

QString Settings::outputFileNameWildcard(bool forceNumber) const
{
    auto fileNameTemplate = outputFileNameTemplate();
    const auto match = s_matchNumberPlaceholder.match(fileNameTemplate);

    if (forceNumber && ! match.hasMatch()) {
        fileNameTemplate.append(QStringLiteral("*"));
    } else {
        fileNameTemplate.replace(match.captured(0), QStringLiteral("*"));
    }

    return finishOutputFileName(fileNameTemplate);
}

QString Settings::outputFileName(int number, bool forceNumber) const
{
    return outputFileName(outputFileNameTemplate(), number, forceNumber);
}

QString Settings::outputFileName(QString fileNameTemplate, int number, bool forceNumber) const
{
    QString numberPlaceholder;
    QString beforeNumber;
    int digits;
    QString afterNumber;

    auto match = s_matchNumberPlaceholder.match(fileNameTemplate);

    // Insert a number placeholder if there's none and we need one
    if (forceNumber && ! match.hasMatch()) {
        fileNameTemplate.append(QStringLiteral("%_nn%"));
        match = s_matchNumberPlaceholder.match(fileNameTemplate);
    }

    if (match.hasMatch()) {
        numberPlaceholder = match.captured(0);
        beforeNumber = match.captured(1);
        digits = match.captured(2).size();
        afterNumber = match.captured(3);
    }

    // If we found one, replace the number placeholder with a number, possibly prepending zeros
    if (! numberPlaceholder.isEmpty()) {
        QString numberString;
        if (number != 1 || ! omitFirstNumber()) {
            numberString = beforeNumber
                           + QStringLiteral("%1").arg(number, digits, 10, QLatin1Char('0'))
                           + afterNumber;
        }
        fileNameTemplate.replace(numberPlaceholder, numberString);
    }

    // Process % characters and possibly insert date and time information
    return finishOutputFileName(fileNameTemplate);
}

void Settings::saveOmitFirstNumber(bool state)
{
    auto group = m_config->group(s_saving);
    group.writeEntry(s_omitFirstNumber, state);
    group.sync();
}

bool Settings::omitFirstNumber() const
{
    const auto group = m_config->group(s_saving);
    return group.readEntry(s_omitFirstNumber, true);
}

void Settings::saveResetAfterSaving(bool state)
{
    auto group = m_config->group(s_saving);
    group.writeEntry(s_resetAfterSaving, state);
    group.sync();
}

bool Settings::resetAfterSaving() const
{
    const auto group = m_config->group(s_saving);
    return group.readEntry(s_resetAfterSaving, false);
}
