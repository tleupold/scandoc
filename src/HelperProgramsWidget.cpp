// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "HelperProgramsWidget.h"
#include "Settings.h"

// KDE includes
#include <KLocalizedString>

// Qt includes
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

// C++ includes
#include <functional>

HelperProgramsWidget::HelperProgramsWidget(SharedObjects *sharedObjects, QWidget *parent)
    : AbstractSettingsWidget(sharedObjects, parent)
{
    QGroupBox *box;
    QVBoxLayout *boxLayout;
    QLabel *label;

    box = new QGroupBox(i18n("Post processing"));
    boxLayout = new QVBoxLayout(box);
    label = new QLabel(i18n(
        "<p>The following command is executed to post process scanned pages.</p>"
        "<p><kbd>%IN%</kbd> is replaced with the original scan (a PNG image) and <kbd>%OUT%</kbd> "
        "is replaced with the image that will actually be used (a JPEG image).</p>"));
    label->setWordWrap(true);
    boxLayout->addWidget(label);
    m_postProcessingCommand = new QLineEdit;
    m_postProcessingCommand->setText(settings()->postProcessingCommand());
    boxLayout->addWidget(m_postProcessingCommand);

    // Check for the deprecated "convert" command
    if (m_postProcessingCommand->text().startsWith(QStringLiteral("convert"))) {
        auto *magickLabel = new QLabel;
        magickLabel->setWordWrap(true);
        magickLabel->setTextInteractionFlags(Qt::TextSelectableByMouse);
        magickLabel->setText(tr(
            "<p><b>Please note:</b> With ImageMagick 7, the \"convert\" command has been "
            "deprecated.<br/>"
            "The current default post processing command would be:</p>"
            "<pre>magick %IN% -level 15%,85% -format JPEG -quality 85 %OUT%</pre>"
            "<p>Please update your settings!</p>"));
        boxLayout->addWidget(magickLabel);
    }

    pageLayout()->addWidget(box);

    box = new QGroupBox(i18n("PDF saving"));
    boxLayout = new QVBoxLayout(box);
    label = new QLabel(i18n(
        "<p>The following command is executed to save all scanned pages as a PDF file.</p>"
        "<p><kbd>%IN%</kbd> is replaced with a list of all post processed scanned pages (JPEG "
        "images). <kbd>%OUT%</kbd> is replaced with the file name of the PDF target.</p>"));
    label->setWordWrap(true);
    boxLayout->addWidget(label);
    m_savePdfCommand = new QLineEdit;
    m_savePdfCommand->setText(settings()->savePdfCommand());
    boxLayout->addWidget(m_savePdfCommand);
    pageLayout()->addWidget(box);

    box = new QGroupBox(i18n("PDF encryption"));
    boxLayout = new QVBoxLayout(box);
    label = new QLabel(i18n(
        "<p>The following command is executed to encrypt a PDF file, if requested.</p>"
        "<p>First, a PDF file is generated using the \"PDF saving\" command. Scandoc will then "
        "prompt for a password. <kbd>%PASS%</kbd> will be replaced with the password. "
        "<kbd>%IN%</kbd> is replaced with the generated PDF file, and <kbd>%OUT%</kbd> with a "
        "temporary file name (which will be the encrypted copy of the original PDF file).</p>"
        "<p>After the encryption succeeded, the original PDF file is replaced with the encrypted "
        "one.</p>"));
    label->setWordWrap(true);
    boxLayout->addWidget(label);
    m_encryptPdfCommand = new QLineEdit;
    m_encryptPdfCommand->setText(settings()->encryptPdfCommand());
    boxLayout->addWidget(m_encryptPdfCommand);
    pageLayout()->addWidget(box);

    box = new QGroupBox(i18n("Sending via mail"));
    boxLayout = new QVBoxLayout(box);
    label = new QLabel(i18n(
        "<p>The following command is executed to send the PDF output as a mail attachment "
        "directly.</p>"
        "<p>First, a PDF file is generated using the \"PDF saving\" command and stored as a "
        "temporary file (that will be cleaned up when the program is closed), and then this file "
        "is passed to the given comand as <kbd>%PDF%</kbd></p>"));
    label->setWordWrap(true);
    boxLayout->addWidget(label);
    m_sendViaMailCommand = new QLineEdit;
    m_sendViaMailCommand->setText(settings()->sendViaMailCommand());
    boxLayout->addWidget(m_sendViaMailCommand);
    pageLayout()->addWidget(box);

    pageLayout()->addStretch();

    addApplyButton();
    connect(m_postProcessingCommand, &QLineEdit::textEdited,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_savePdfCommand, &QLineEdit::textEdited,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_encryptPdfCommand, &QLineEdit::textEdited,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
    connect(m_sendViaMailCommand, &QLineEdit::textEdited,
            this, std::bind(&QPushButton::setEnabled, applyButton(), true));
}

void HelperProgramsWidget::saveSettings()
{
    settings()->savePostProcessingCommand(m_postProcessingCommand->text().simplified());
    settings()->saveSavePdfCommand(m_savePdfCommand->text().simplified());
    settings()->saveEncryptPdfCommand(m_encryptPdfCommand->text().simplified());
    settings()->saveSendViaMailCommand(m_sendViaMailCommand->text().simplified());
    settings()->saveCommandsRevision();
}
