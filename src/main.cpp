// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "SharedObjects.h"
#include "MainWindow.h"

// KDE includes
#include <KCrash>
#include <KLocalizedString>

// Qt includes
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps, true);
#endif

    KLocalizedString::setApplicationDomain("scandoc");
    KCrash::initialize();

    SharedObjects sharedObjects;

    MainWindow mainWindow(&sharedObjects);
    mainWindow.show();

    return app.exec();
}
