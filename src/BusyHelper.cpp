// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "BusyHelper.h"

// Qt includes
#include <QApplication>
#include <QWidget>

BusyHelper::BusyHelper(QObject *parent) : QObject(parent)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    const auto topLevelWidgets = QApplication::topLevelWidgets();
    for(QWidget *widget : topLevelWidgets){
        if (widget->inherits("QMainWindow")) {
            m_mainWindow = widget;
            m_mainWindow->setEnabled(false);
            QApplication::processEvents();
            break;
        }
    }
}

BusyHelper::~BusyHelper()
{
    m_mainWindow->setEnabled(true);
    QApplication::restoreOverrideCursor();
}
