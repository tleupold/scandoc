// SPDX-FileCopyrightText: 2022-2023 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef SCANDOC_H
#define SCANDOC_H

namespace Scandoc
{

enum ScanMode {
    DefaultScanMode,
    OneSidedScanMode,
    DuplexScanMode
};

enum SaveMode {
    SavePdf,
    SaveProcessedPage,
    SaveUnprocessedPage
};

}

#endif // SCANDOC_H
