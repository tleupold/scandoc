// SPDX-FileCopyrightText: 2022-2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

// Local includes
#include "ScannerSettingsWidget.h"
#include "SharedObjects.h"
#include "Settings.h"
#include "BusyHelper.h"
#include "Logging.h"
#include "ValuesComboBox.h"

// KDE includes
#include <KLocalizedString>
#include <KSaneCore/DeviceInformation>
#include <KSaneCore/Option>
#include <KSaneCore/Interface>

// Qt includes
#include <QApplication>
#include <QMessageBox>
#include <QDebug>
#include <QVBoxLayout>
#include <QScrollArea>
#include <QGridLayout>
#include <QLabel>
#include <QComboBox>
#include <QSpinBox>
#include <QCheckBox>
#include <QDoubleSpinBox>
#include <QPushButton>
#include <QTimer>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QTimer>

static const QString s_scanMode       = QStringLiteral("mode");
static const QString s_bitDepth       = QStringLiteral("depth");
static const QString s_resolution     = QStringLiteral("resolution");
static const QString s_source         = QStringLiteral("source");
static const QString s_pageSize       = QStringLiteral("KSane::PageSize");
static const QString s_tlX            = QStringLiteral("tl-x");
static const QString s_tlY            = QStringLiteral("tl-y");
static const QString s_brX            = QStringLiteral("br-x");
static const QString s_brY            = QStringLiteral("br-y");
static const QString s_batchMode      = QStringLiteral("KSane::BatchMode");
static const QString s_batchTimeDelay = QStringLiteral("KSane::BatchTimeDelay");

static const QString s_hasDuplex       = QStringLiteral("hasDuplex");
static const QString s_oneSidedSource  = QStringLiteral("oneSidedSource");
static const QString s_duplexSource    = QStringLiteral("duplexSource");
static const QString s_rotateEvenPages = QStringLiteral("rotateEvenPages");

static const QString s_true = QStringLiteral("true");
static const QString s_false = QStringLiteral("false");

ScannerSettingsWidget::ScannerSettingsWidget(SharedObjects *sharedObjects, QWidget *parent)
    : AbstractSettingsWidget(sharedObjects, parent),
      m_saneInterface(sharedObjects->saneInterface())
{
    connect(m_saneInterface, &KSaneCore::Interface::scanFinished,
            this, &ScannerSettingsWidget::checkRestoreSource);

    auto *layout = new QHBoxLayout;
    mainLayout()->insertLayout(0, layout);

    layout->addWidget(new QLabel(i18n("Used scanner:")));

    m_devices = new QComboBox;
    layout->addWidget(m_devices);
    connect(m_devices, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &ScannerSettingsWidget::openDevice);

    connect(m_saneInterface, &KSaneCore::Interface::availableDevices,
            this, &ScannerSettingsWidget::availableDevicesChanged);

    layout->addStretch();

    addApplyButton();
    applyButton()->setText(i18n("Save settings"));

    QTimer::singleShot(300, this, &ScannerSettingsWidget::reloadDevicesList);
}

void ScannerSettingsWidget::reloadDevicesList()
{
    Q_EMIT showMessage(i18n("Checking for scanner devices ..."));
    QApplication::setOverrideCursor(Qt::WaitCursor);
    m_saneInterface->reloadDevicesList();
}

void ScannerSettingsWidget::openDevice(int index)
{
    if (index == -1) {
        applyButton()->setEnabled(false);
        return;
    }

    const auto scanner = m_devices->currentText();
    Q_EMIT showMessage(i18n("Opening scanner \"%1\"", scanner));

    BusyHelper busyHelper;

    saveSettings();
    m_saneInterface->closeDevice();

    if (m_saneInterface->openDevice(m_devices->currentData().toString())
        != KSaneCore::Interface::OpeningSucceeded) {

        QTimer::singleShot(0, this, [this, scanner]
        {
            QMessageBox::warning(this, i18n("Opening device failed"),
                                 i18n("Could not open scanner \"%1\"", scanner));
        });
        m_devices->setCurrentIndex(-1);
        Q_EMIT scannerOpened(false);
        Q_EMIT showMessage(i18n("Opening scanner \"%1\" failed", scanner));
        return;
    }

    Q_EMIT showMessage(i18n("Opened scanner \"%1\"", scanner));
    applyButton()->setEnabled(true);

    const auto savedSettings = settings()->scannerSettings(scanner);
    if (! savedSettings.isEmpty()) {
        m_saneInterface->setOptionsMap(savedSettings);
    } else {
        Q_EMIT showMe();
        QTimer::singleShot(0, this, [this, scanner]
        {
            QMessageBox::information(this, i18n("Please choose scanner settings"),
                i18n("<p>There are no saved settings for the scanner \"%1\". The default settings "
                     "have been loaded.</p>"
                     "<p>Please review it's settings, select the proper source, page size etc."
                     "</p>", scanner));
        });
    }

    refresh();
    Q_EMIT scannerOpened(true);
    Q_EMIT updateScanMode(settings()->lastScanMode(currentDeviceString()));
}

void ScannerSettingsWidget::availableDevicesChanged(
    const QList<KSaneCore::DeviceInformation *> &deviceList)
{
    QApplication::restoreOverrideCursor();
    Q_EMIT showMessage(i18n("Checking for scanner devices finished"));

    const auto devicesCount = deviceList.count();

    if (devicesCount == 0) {
        applyButton()->setEnabled(false);
        QMessageBox::warning(this, i18n("No devices found"),
                             i18n("Could not find any scanner devices"));

        auto *widget = new QWidget;
        auto *layout = new QVBoxLayout(widget);
        layout->addStretch();

        auto *buttonLayout = new QHBoxLayout;
        layout->addLayout(buttonLayout);
        buttonLayout->addStretch();
        auto *button = new QPushButton(i18n("Reload devices list"));
        button->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogRetryButton));
        connect(button, &QPushButton::clicked, this, &ScannerSettingsWidget::reloadDevicesList);
        buttonLayout->addWidget(button);
        buttonLayout->addStretch();

        auto *label = new QLabel(i18n(
            "Alas, this does not always work relibably (dependant of the SANE backend being used). "
            "If reloading the devices list does not expose devices added in the meantime please "
            "simply restart Scandoc."));
        label->setWordWrap(true);
        layout->addWidget(label);

        layout->addStretch();
        scrollArea()->setWidget(widget);
        widget->show();

        Q_EMIT showMe();
    }

    m_devices->blockSignals(true);
    m_devices->clear();

    for (const auto &device : deviceList) {
        m_devices->addItem(QStringLiteral("%1 %2").arg(device->vendor(), device->model()),
                           device->name());
    }

    m_devices->setCurrentIndex(-1);
    m_devices->blockSignals(false);

    if (devicesCount == 1) {
        m_devices->setCurrentIndex(0);
    } else if (devicesCount > 1) {
        Q_EMIT showMe();
        QMessageBox::information(this, i18n("Multiple decives found"),
                                 i18n("Please select the scanner to use"));
    }
}

QComboBox *ScannerSettingsWidget::createSourceComboBox() const
{
    auto *combo = new QComboBox;
    for (int i = 0; i < m_source->count(); i++) {
        combo->addItem(m_source->itemText(i));
    }
    return combo;
}

void ScannerSettingsWidget::refresh()
{
    auto *widget = new QWidget;
    auto *wrapperLayout = new QVBoxLayout(widget);

    QGroupBox *box;
    QGridLayout *layout;
    int row;
    m_handledOptions.clear();

    // Add some known default options first

    box = new QGroupBox(i18n("Basic settings"));
    layout = new QGridLayout(box);
    row = 0;

    // Scan mode, bit depth, resolution, source
    addNamedOption(s_scanMode, layout, row);
    addNamedOption(s_bitDepth, layout, row);
    addNamedOption(s_resolution, layout, row);
    auto sourceComponents = addNamedOption(s_source, layout, row);
    if (sourceComponents.control != nullptr) {
        m_source = qobject_cast<QComboBox *>(sourceComponents.control);
    }

    // Page size

    auto *manualSizeWidget = new QWidget;
    auto *manualSizeLayout = new QGridLayout(manualSizeWidget);
    manualSizeLayout->setContentsMargins(20, 0, 0, 0);
    int manualSizeRow = 0;

    // Page size selector

    auto pageSizeComponents = addNamedOption(s_pageSize, layout, row);
    auto *control = qobject_cast<QComboBox *>(pageSizeComponents.control);
    connect(control, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, [manualSizeWidget](int index)
            {
                manualSizeWidget->setVisible(index == 0);
            });

    // Possibly hide the manual page size widget after loading has finished
    QTimer::singleShot(0, [manualSizeWidget, control]
    {
        manualSizeWidget->setVisible(control->currentIndex() == 0);
    });

    // tlX, tlY, brX and brY

    for (const auto &name : { s_tlX, s_tlY, s_brX, s_brY }) {
        auto sizeComponents = addNamedOption(name, manualSizeLayout, manualSizeRow);
        if (sizeComponents.option != nullptr) {
            auto *control = qobject_cast<QDoubleSpinBox *>(sizeComponents.control);
            connect(sizeComponents.option, &KSaneCore::Option::valueChanged,
                    this, [control](const QVariant &value)
                    {
                        control->setValue(value.toDouble());
                    });
        }
    }

    layout->addWidget(manualSizeWidget, row++, 0, 1, 2);

    wrapperLayout->addWidget(box);

    // Possibly add the "Duplex scanning" box

    if (m_source != nullptr) {
        box = new QGroupBox(i18n("Duplex scanning"));
        auto *boxLayout = new QVBoxLayout(box);

        m_hasDuplex = new QCheckBox(i18n("This device has a duplex scanning unit"));
        boxLayout->addWidget(m_hasDuplex);
        connect(m_hasDuplex, &QCheckBox::toggled, this, &ScannerSettingsWidget::duplexEnabled);

        auto *duplexWidget = new QWidget;
        duplexWidget->hide();
        auto *duplexLayout = new QGridLayout(duplexWidget);
        connect(m_hasDuplex, &QCheckBox::toggled, duplexWidget, &QWidget::setVisible);

        auto *label = new QLabel(i18n("The following sources will be selected automatically when "
                                      "starting a \"one-sided\" or \"duplex\" scan on the "
                                      "\"Scanned pages\" tab:"));
        label->setWordWrap(true);
        duplexLayout->addWidget(label, 0, 0, 1, 2);

        duplexLayout->addWidget(new QLabel(i18n("One-sided source")), 1, 0);
        m_oneSidedSource = createSourceComboBox();
        duplexLayout->addWidget(m_oneSidedSource, 1, 1);

        duplexLayout->addWidget(new QLabel(i18n("Duplex source")), 2, 0);
        m_duplexSource = createSourceComboBox();
        duplexLayout->addWidget(m_duplexSource, 2, 1);

        m_rotateEvenPages = new QCheckBox(i18n("Rotate all even pages by 180° when duplex "
                                               "scanning"));
        connect(m_rotateEvenPages, &QCheckBox::toggled, settings(), &Settings::setRotateEvenPages);
        duplexLayout->addWidget(m_rotateEvenPages, 3, 1);

        boxLayout->addWidget(duplexWidget);
        wrapperLayout->addWidget(box);

        // Restore the saved duplex settings if the dataset is valid

        const auto scandocSettings = settings()->scandocSettings(m_devices->currentText());
        if (   scandocSettings.contains(s_hasDuplex)
            && (   scandocSettings.value(s_hasDuplex) == s_true
                || scandocSettings.value(s_hasDuplex) == s_false)
            && scandocSettings.contains(s_oneSidedSource)
            && scandocSettings.contains(s_duplexSource)
            && scandocSettings.contains(s_rotateEvenPages)
            && m_oneSidedSource->findText(scandocSettings.value(s_oneSidedSource)) != -1
            && m_duplexSource->findText(scandocSettings.value(s_duplexSource)) != -1
            && (   scandocSettings.value(s_rotateEvenPages) == s_true
                || scandocSettings.value(s_rotateEvenPages) == s_false)) {

            m_hasDuplex->setChecked(scandocSettings.value(s_hasDuplex) == s_true);
            m_oneSidedSource->setCurrentIndex(
                m_oneSidedSource->findText(scandocSettings.value(s_oneSidedSource)));
            m_duplexSource->setCurrentIndex(
                m_duplexSource->findText(scandocSettings.value(s_duplexSource)));
            m_rotateEvenPages->setChecked(scandocSettings.value(s_rotateEvenPages) == s_true);
        }
    }

    // Batch scanning

    box = new QGroupBox(i18n("Batch scanning"));
    layout = new QGridLayout(box);
    row = 0;

    auto batchModeComponents = addNamedOption(s_batchMode, layout, row);
    auto batchTimeDelayComponents = addNamedOption(s_batchTimeDelay, layout, row);
    if (batchModeComponents.control != nullptr && batchTimeDelayComponents.control != nullptr) {
        auto *batchModeComponentsControl = qobject_cast<QCheckBox *>(batchModeComponents.control);
        auto *batchTimeDelayComponentsControl
            = qobject_cast<QSpinBox *>(batchTimeDelayComponents.control);
        connect(batchModeComponentsControl, &QCheckBox::toggled,
                batchTimeDelayComponentsControl, &QWidget::setEnabled);
        batchTimeDelayComponentsControl->setEnabled(batchModeComponentsControl->isChecked());
    }

    if (layout->count() > 0) {
        wrapperLayout->addWidget(box);
    } else {
        box->deleteLater();
    }

    // Display all remaining options

    box = new QGroupBox(i18n("Other settings"));
    layout = new QGridLayout(box);
    row = 0;

    const auto options = m_saneInterface->getOptionsList();

    for (const auto &option : options) {
        if (m_handledOptions.contains(option)) {
            continue;
        }

        auto optionComponents = getOptionComponents(option);
        if (optionComponents.control != nullptr) {
            addOptionWidgets(layout, optionComponents, row);
        }
    }

    if (layout->count() > 0) {
        wrapperLayout->addWidget(box);
    } else {
        box->deleteLater();
    }

    // Display the widget

    wrapperLayout->addStretch();
    scrollArea()->setWidget(widget);
    widget->show();
}

ScannerSettingsWidget::OptionComponents ScannerSettingsWidget::addNamedOption(
    const QString &name, QGridLayout *layout, int &row)
{
    auto *option = m_saneInterface->getOption(name);
    if (option == nullptr || option->state() != KSaneCore::Option::StateActive) {
        return OptionComponents();
    }

    m_handledOptions.append(option);
    auto optionComponents = getOptionComponents(option);
    addOptionWidgets(layout, optionComponents, row);

    return optionComponents;
}

void ScannerSettingsWidget::addOptionWidgets(QGridLayout *layout,
                                             OptionComponents &optionComponents, int &row)
{
    if (optionComponents.label != nullptr) {
        layout->addWidget(optionComponents.label, row, 0);
        layout->addWidget(optionComponents.control, row++, 1);
    } else {
        layout->addWidget(optionComponents.control, row++, 0, 1, 2);
    }
}

ScannerSettingsWidget::OptionComponents ScannerSettingsWidget::getOptionComponents(
    KSaneCore::Option *option) const
{
    OptionComponents optionComponents;

    if (option->state() != KSaneCore::Option::StateActive) {
        return optionComponents;
    }

    switch (option->type()) {

    case KSaneCore::Option::TypeValueList: {
        optionComponents.label = new QLabel(option->title());
        auto *optionWidget = new ValuesComboBox;
        const auto valueList = option->valueList();
        for (const auto &value : valueList) {
            optionWidget->addItem(value.toString(), value);
        }
        optionWidget->setCurrentIndex(optionWidget->findText(option->value().toString()));
        optionWidget->setToolTip(option->description());
        connect(optionWidget, &ValuesComboBox::currentValueChanged,
                option, &KSaneCore::Option::setValue);
        optionComponents.control = optionWidget;
        break;
    }

    case KSaneCore::Option::TypeInteger: {
        optionComponents.label = new QLabel(option->title());
        auto *optionWidget = new QSpinBox;
        optionWidget->setMinimum(option->minimumValue().toInt());
        optionWidget->setMaximum(option->maximumValue().toInt());
        optionWidget->setSingleStep(option->stepValue().toInt());
        optionWidget->setValue(option->value().toInt());
        optionWidget->setToolTip(option->description());
        optionWidget->setSuffix(unitSuffix(option));
        connect(optionWidget, &QSpinBox::valueChanged,
                option, &KSaneCore::Option::setValue);
        optionComponents.control = optionWidget;
        break;
    }

    case KSaneCore::Option::TypeBool: {
        auto *optionWidget = new QCheckBox(option->title());
        optionWidget->setChecked(option->value().toBool());
        optionWidget->setToolTip(option->description());
        connect(optionWidget, &QCheckBox::toggled, option, &KSaneCore::Option::setValue);
        optionComponents.control = optionWidget;
        break;
    }

    case KSaneCore::Option::TypeDouble: {
        optionComponents.label = new QLabel(option->title());
        auto *optionWidget = new QDoubleSpinBox;
        optionWidget->setMinimum(option->minimumValue().toDouble());
        optionWidget->setMaximum(option->maximumValue().toDouble());
        optionWidget->setSingleStep(option->stepValue().toDouble());
        optionWidget->setValue(option->value().toDouble());
        optionWidget->setToolTip(option->description());
        optionWidget->setSuffix(unitSuffix(option));
        connect(optionWidget, &QDoubleSpinBox::valueChanged,
                option, &KSaneCore::Option::setValue);
        optionComponents.control = optionWidget;
        break;
    }

    case KSaneCore::Option::TypeGamma: {
        optionComponents.label = new QLabel(option->title());
        auto *optionWidget = new QDoubleSpinBox;
        optionWidget->setMinimum(option->minimumValue().toDouble());
        optionWidget->setMaximum(option->maximumValue().toDouble());
        optionWidget->setSingleStep(option->stepValue().toDouble());
        optionWidget->setValue(option->value().toDouble());
        optionWidget->setToolTip(option->description());
        optionWidget->setSuffix(unitSuffix(option));
        connect(optionWidget, &QDoubleSpinBox::valueChanged,
                option, &KSaneCore::Option::setValue);
        optionComponents.control = optionWidget;
        break;
    }

    case KSaneCore::Option::TypeDetectFail:
    case KSaneCore::Option::TypeAction:
    default:
        qCDebug(scandocLog) << "Skipped option:";
        qCDebug(scandocLog) << "    description      :" << option->description();
        qCDebug(scandocLog) << "    internalValueList:" << option->internalValueList();
        qCDebug(scandocLog) << "    maximumValue     :" << option->maximumValue();
        qCDebug(scandocLog) << "    minimumValue     :" << option->minimumValue();
        qCDebug(scandocLog) << "    name             :" << option->name();
        qCDebug(scandocLog) << "    stepValue        :" << option->stepValue();
        qCDebug(scandocLog) << "    title            :" << option->title();
        qCDebug(scandocLog) << "    type             :" << option->type();
        qCDebug(scandocLog) << "    value            :" << option->value();
        qCDebug(scandocLog) << "    valueList        :" << option->valueList();
        qCDebug(scandocLog) << "    valueSize        :" << option->valueSize();
        qCDebug(scandocLog) << "    valueUnit        :" << option->valueUnit();
        return optionComponents;
    }

    optionComponents.option = option;
    return optionComponents;
}

QString ScannerSettingsWidget::unitSuffix(KSaneCore::Option *option) const
{
    switch (option->valueUnit()) {
    case KSaneCore::Option::UnitSecond:
        return i18nc("seconds as a unit suffix", " s");
    case KSaneCore::Option::UnitMicroSecond:
        return i18nc("microseconds as a unit suffix", " µs");
    case KSaneCore::Option::UnitDPI:
        return i18nc("dots per inch as a unit suffix", " dpi");
    case KSaneCore::Option::UnitMilliMeter:
        return i18nc("millimeter as a unit suffix", " mm");
    case KSaneCore::Option::UnitPercent:
        return i18nc("percent as a unit suffix", " %");
    case KSaneCore::Option::UnitPixel:
        return i18nc("pixel as a unit suffix", " px");
    case KSaneCore::Option::UnitBit:
        return i18nc("bit as a unit suffix", " bit");
    case KSaneCore::Option::UnitNone:
    default:
        return QString();
    }
}

QString ScannerSettingsWidget::currentDeviceString() const
{
    return QStringLiteral("%1 %2").arg(m_saneInterface->deviceVendor(),
                                       m_saneInterface->deviceModel()).trimmed();
}

void ScannerSettingsWidget::saveSettings()
{

    const auto deviceId = currentDeviceString();
    if (deviceId.isEmpty()) {
        return;
    }

    settings()->saveScannerSettings(deviceId, m_saneInterface->getOptionsMap());

    QMap<QString, QString> scandocSetttings;
    if (m_source != nullptr) {
        scandocSetttings.insert(s_hasDuplex,      m_hasDuplex->isChecked() ? s_true : s_false);
        scandocSetttings.insert(s_oneSidedSource, m_oneSidedSource->currentText());
        scandocSetttings.insert(s_duplexSource,   m_duplexSource->currentText());
        scandocSetttings.insert(s_rotateEvenPages,
                                m_rotateEvenPages->isChecked() ? s_true : s_false);
    }
    settings()->saveScandocSettings(deviceId, scandocSetttings);

    applyButton()->setEnabled(true);

    QMessageBox::information(this, i18n("Scanner settings"), i18n("Settings saved!"));
}

void ScannerSettingsWidget::scanModeChanged(Scandoc::ScanMode mode)
{
    settings()->saveLastScanMode(currentDeviceString(), mode);
}

void ScannerSettingsWidget::startScan(Scandoc::ScanMode mode)
{
    if (mode == Scandoc::OneSidedScanMode || mode == Scandoc::DuplexScanMode) {
        m_restoreSource = true;
        m_lastSource = m_source->currentIndex();
    }

    if (mode == Scandoc::OneSidedScanMode) {
        m_source->setCurrentIndex(m_oneSidedSource->currentIndex());
    } else if (mode == Scandoc::DuplexScanMode) {
        m_source->setCurrentIndex(m_duplexSource->currentIndex());
    }

    m_saneInterface->startScan();
}

void ScannerSettingsWidget::checkRestoreSource()
{
    if (m_restoreSource) {
        m_restoreSource = false;
        m_source->setCurrentIndex(m_lastSource);
    }
}
