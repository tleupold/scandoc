// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

// Qt includes
#include <QMainWindow>

// Local classes
class SharedObjects;
class Settings;
class ScannedPagesWidget;
class ScannerSettingsWidget;
class HelperProgramsWidget;
class SavingWidget;

// Qt classes
class QTabWidget;
class QStatusBar;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(SharedObjects *sharedObjects);

protected:
    void closeEvent(QCloseEvent *event) override;

private Q_SLOTS:
    void showMessage(const QString &message);

private: // Variables
    Settings *m_settings;
    QTabWidget *m_tabWidget;
    QStatusBar *m_statusBar;
    ScannedPagesWidget *m_scannedPagesWidget;
    ScannerSettingsWidget *m_scannerSettingsWidget;
    HelperProgramsWidget *m_helperProgramsWidget;
    SavingWidget *m_savingWidget;

};

#endif // MAINWINDOW_H
