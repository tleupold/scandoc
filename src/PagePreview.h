// SPDX-FileCopyrightText: 2022 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL

#ifndef PAGEPREVIEW_H
#define PAGEPREVIEW_H

// Qt includes
#include <QWidget>
#include <QImage>

// Qt classes
class QScrollArea;
class QLabel;
class QToolButton;

class PagePreview : public QWidget
{
    Q_OBJECT

public:
    explicit PagePreview(QWidget *parent = nullptr);
    void setImage(const QString &path);

protected:
    void resizeEvent(QResizeEvent *event) override;

private Q_SLOTS:
    void zoomToFit();
    void zoomToOriginal();
    void zoomIn();
    void zoomOut();

private: // Functions
    void updatePreview();
    void toggleZoomButtons();
    QSize maximalViewSize() const;

private: // Variables
    QScrollArea *m_scrollArea;
    QLabel *m_preview;
    QImage m_image;
    QToolButton *m_zoomToFitButton;
    QToolButton *m_zoomToOriginalButton;
    QToolButton *m_zoomInButton;
    QToolButton *m_zoomOutButton;

};

#endif // PAGEPREVIEW_H
